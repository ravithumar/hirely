<?php

if(!function_exists('assets')){

	function assets($file)

	{

		return base_url('assets/').$file;

	}

}

if(!function_exists('assets_css')){

	function assets_css($file)

	{

		return base_url('assets/css/').$file.'.css';

	}

}

if(!function_exists('assets_js')){

	function assets_js($file)

	{

		return base_url('assets/js/').$file.'.js';

	}

}

if(!function_exists('assets_image')){

	function assets_image($file)

	{

		return base_url('assets/image/').$file;

	}

}

if(!function_exists('assets_upload')){

	function assets_upload($file)

	{

		return base_url('assets/upload/').$file;

	}

}

if(!function_exists('validation_errors_response_web')){

    function validation_errors_response_web()

    {

    	$err_array=array();

    	$err_str="";

    	$err_str=str_replace(array('<p>','</p>'),array('|',''),trim(validation_errors()));

    	$err_str=ltrim($err_str,'|');

    	$err_str=rtrim($err_str,'|');

    	// $err_array=explode('|',$err_str);

    	// $err_array = array_filter($err_array);

    	return $err_array;

    }

}

/*

* Additional :

*/

if(!function_exists('assets_less')){

	function assets_less($file)

	{

		return base_url('assets/less/').$file.'.less';

	}

}

if(!function_exists('assets_sass')){

	function assets_sass($file)

	{

		return base_url('assets/sass/').$file.'.sass';

	}

}

if(!function_exists('to_json')){

	function to_json($array)

	{

		

	}

}

if(!function_exists('config')){

	function config($key)

	{

		$CI = get_instance();

		$CI->load->model('Config');

		$config = Config::wherePath($key)->first();

		return $config->value;

	}

}

if(!function_exists('config_set')){

	function config_set($key_array)

	{

		$CI = get_instance();

	$CI->load->model('Config');

		foreach ($key_array as $key => $value) {

			$update['value']=$value;

			

			Config::wherePath($key)->update($update);

		}

		return true;

	}

}

if(!function_exists('image_upload')){

	 function image_upload($file,$path='',$enc=FALSE){

	 	$CI = get_instance();

	 	

	 	if(!is_dir("./assets/".$path))

	 	{

	 		

	 		mkdir("./assets/".$path,0777,TRUE);

	 	}

		$config = array(

			'upload_path' => "./assets/".$path,

			'allowed_types' => "gif|jpg|png|jpeg|pdf",

			'overwrite' => TRUE,

			'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)

			'remove_spaces' => TRUE,

            'encrypt_name' => $enc,

            'file_ext_tolower' =>TRUE

		);

		$CI->load->library('upload', $config);

		$CI->upload->initialize($config);

		if($CI->upload->do_upload($file))

		{

			$data = $CI->upload->data();

			$data['status']=true;

			$data['uploaded_path'] = "assets/" . $path . $data['file_name'];			

		}

		else

		{

			$data['error'] = $CI->upload->display_errors();

			$data['status']=false;

		}

		return $data;

	}

}

if(!function_exists('CsvUpload')){

	 function CsvUpload($file,$path='',$enc=FALSE){

	 	$CI = get_instance();

	 	

	 	if(!is_dir("./assets/".$path))

	 	{

	 		

	 		mkdir("./assets/".$path,0777,TRUE);

	 	}

		$config = array(

			'upload_path' => "./assets/".$path,

			'allowed_types' => "csv|xlsx",

			'overwrite' => TRUE,

			'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)

			'remove_spaces' => TRUE,

            'encrypt_name' => $enc,

            'file_ext_tolower' =>TRUE

		);

		$CI->load->library('upload', $config);

		$CI->upload->initialize($config);

		if($CI->upload->do_upload($file))

		{

			$data = $CI->upload->data();

			$data['status']=true;

			

		}

		else

		{

			$data['error'] = $CI->upload->display_errors();

			$data['status']=false;

		}

		return $data;

	}

}

if(!function_exists('__')){

	function __(){

		$args = func_get_args();

		if(count($args) >= 1){

			$CI = get_instance();

			$str = $args[0];

			$nstr = $str;

			$fstr = $CI->lang->line($str);

			if($fstr != null){

				$nstr = $fstr;

			}

			if(count($args) >= 2){

				array_shift($args);

				return vsprintf($nstr,$args);

			}

			return $nstr;

		}

		return null;

	}

}

if(!function_exists('__timeago')){

	function __timeago($datetime, $full = false) {

		    $now = new DateTime;

		    $ago = new DateTime($datetime);

		    $diff = $now->diff($ago);

		    $diff->w = floor($diff->d / 7);

		    $diff->d -= $diff->w * 7;

		    $string = array(

		        'y' => 'year',

		        'm' => 'month',

		        'w' => 'week',

		        'd' => 'day',

		        'h' => 'hour',

		        'i' => 'minute',

		        's' => 'second',

		    );

		    foreach ($string as $k => &$v) {

		        if ($diff->$k) {

		            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');

		        } else {

		            unset($string[$k]);

		        }

		    }

		    if (!$full) $string = array_slice($string, 0, 1);

		    return $string ? implode(', ', $string) . ' ago' : 'just now';

		}

}

if (!function_exists('__date'))

{	

	function __date($date,$format=false)

	{

		if($format)

		{

			$date=date_create($date);

			return date_format($date,"Y-m-d");

		}

		else

		{

			$date=date_create($date);

			return date_format($date,config('date_format'));

		}

	}

}

if (!function_exists('base64ToImage'))

{	

	function base64ToImage($image,$dir)

	{

        $img     = $image;

        $img     = str_replace('data:image/png;base64,', '', $img);

        $img     = str_replace('data:image/jpeg;base64,', '', $img);

        $img     = str_replace(' ', '+', $img);

        $data    = base64_decode($img);

        $time    = time() . '.png';

        $file    = 'assets/'.$dir.$time;

        $success = file_put_contents($file, $data);

        if($success)

        {

        	return $time;

        }

        else

        {

        	return "";

        }

	}

}

if (!function_exists('_pre'))

{	

	function _pre($array)

	{

        echo "<pre>";

        print_r($array);

        echo "<pre>";

        exit;

	}

}

if(!function_exists('show_general'))

{

	function show_general($message="")

	{

		$CI = get_instance();

		$data['heading'] = "Auth error";

		$data['message'] = $message;

		$CI->load->view('errors/html/error_general',$data);

	}

}

if(!function_exists('routeLink'))

{

	function routeLink($url)

	{

		return base_url($url);

	}

}

if(!function_exists('generateString'))

{

	function generateString() {

		$strength = 6;

		$input = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

	    $input_length = strlen($input);

	    $random_string = '';

	    for($i = 0; $i < $strength; $i++) {

	        $random_character = $input[mt_rand(0, $input_length - 1)];

	        $random_string .= $random_character;

	    }

	    return $random_string;

	}

}

if(!function_exists('renderFront'))

{

	function renderFront($page = null,$params = null, $return = false)

	{

		$CI = get_instance();

        $CI->load->view('front/layout/header',[]);

        if($page != null){

            $CI->load->view('front/pages/'.$page,$params,$return);

        }

        $CI->load->view('front/layout/footer',$params);

	}

}

if(!function_exists('validation_errors_response'))

{

	  function validation_errors_response()

    {

    	$err_array=array();

    	$err_str="";

    	$err_str=str_replace(array('<p>','</p>'),array('|',''),trim(validation_errors()));

    	$err_str=ltrim($err_str,'|');

    	$err_str=rtrim($err_str,'|');

    	$err_array=explode('|',$err_str);

    	$err_array = array_filter($err_array);

    	return $err_array;

    }

}



//========================Last Query===================================

if (!function_exists('lq')) {

    function lq() {

        $CI = &get_instance();

        return $CI->db->last_query();

    }

}

//========================Last Query===================================

//========================Print===================================

if (!function_exists('_pre')) {

    function _pre($array) {

        echo "<pre>";

        print_r($array);

        echo "<pre>";

        exit;

    }

}

//========================Print===================================



//========================Current Date===================================

if (!function_exists('current_date')) {

	function current_date() {

		return date('Y-m-d H:i:s');

	}

}

//========================Current Date===================================



//========================Random Password===================================

if (!function_exists('randomPassword')) {

	function randomPassword() 

	{

	    $alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';

	    $pass = array(); //remember to declare $pass as an array

	    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache

	    for ($i = 0; $i < 8; $i++) {

	        $n = rand(0, $alphaLength);

	        $pass[] = $alphabet[$n];

	    }

	    return implode($pass); //turn the array into a string

	}	

}

//========================Random Password===================================

//======================== web site render ===================================

if (!function_exists('render')) {

	function render($page = null,$params = null, $return = false) 

	{

		$CI = &get_instance();        

        // $params['body_class'] = $CI->_generate_body_class();

        $CI->load->view('front/layout/header',$params);

        if($page != null){

            $CI->load->view('front/pages/'.$page,$params,$return);

        }

        $CI->load->view('front/layout/footer',$params);

    }

}
//======================== web site render ===================================

//======================== Users Id ===================================
if (!function_exists('users_ids')) {
	function users_ids() {
		$CI = &get_instance();
		$CI->db->select('GROUP_CONCAT(users_groups.user_id SEPARATOR ",") as users_id');
		$CI->db->join('users_groups', 'users_groups.group_id = groups.id ', 'join');
		$CI->db->where('groups.name', $CI->config->item('users'));
		$users_ids = $CI->db->get('groups');
		if ($users_ids->num_rows() > 0) {
			return $users_ids->row();
		} else {
			return null;
		}
	}
}
//======================== Users Id ===================================

//======================== Provider Id ===================================
if (!function_exists('provider_ids')) {
	function provider_ids() {
		$CI = &get_instance();
		$CI->db->select('GROUP_CONCAT(users_groups.user_id SEPARATOR ",") as provider_id');
		$CI->db->join('users_groups', 'users_groups.group_id = groups.id ', 'join');
		$CI->db->where('groups.name', $CI->config->item('provider'));
		$provider_ids = $CI->db->get('groups');
		if ($provider_ids->num_rows() > 0) {
			return $provider_ids->row();
		} else {
			return null;
		}
	}
}
//======================== Provider Id ===================================

//======================== validate_expirydate ===================================
if (!function_exists('validate_expirydate')) {
	function validate_expirydate($expiry_date = NULL) {
		$return = false;
		$expiry_date_array = explode('/', $expiry_date);
		if (count($expiry_date_array) == 2) {
			if (checkdate($expiry_date_array[0], '01', $expiry_date_array[1])) {
				$expiry_date_obj = DateTime::createFromFormat('d/m/Y H:i:s', "01/" . $expiry_date_array[0] . "/" . $expiry_date_array[1] . " 00:00:00");
				$expiry_date_obj = new DateTime($expiry_date_obj->format("Y-m-t"));
				$my_date = date('d/m/Y');
				$today = DateTime::createFromFormat('d/m/Y H:i:s', $my_date . " 00:00:00");
				if ($expiry_date_obj >= $today) {
					$return = true;
				}
			}
		}
		return $return;
	}
}
//======================== validate_expirydate ===================================

//======================== encrypt ===================================

if (!function_exists('encrypt')) {
	function encrypt($string) {
		$CI = &get_instance();
		$key = $CI->config->item('custom_encryption_key');
		$result = '';
		for ($i = 0, $k = strlen($string); $i < $k; $i++) {
			$char = substr($string, $i, 1);
			$keychar = substr($key, ($i % strlen($key)) - 1, 1);
			$char = chr(ord($char) + ord($keychar));
			$result .= $char;
		}
		return base64_encode($result);
	}
}
//======================== encrypt ===================================

//======================== decrypt ===================================

if (!function_exists('decrypt')) {
	function decrypt($string) {
		//print_r($string);exit;
		$CI = &get_instance();
		$key = $CI->config->item('custom_encryption_key');
		$result = '';
		$string = base64_decode($string);
		for ($i = 0, $k = strlen($string); $i < $k; $i++) {
			$char = substr($string, $i, 1);
			$keychar = substr($key, ($i % strlen($key)) - 1, 1);
			$char = chr(ord($char) - ord($keychar));
			$result .= $char;
		}
		return $result;
	}
}
//======================== decrypt ===================================

//======================== validate_customer_card ===================================
if (!function_exists('validate_customer_card')) {
	function validate_customer_card($card_number = NULL) {
		if (isset($card_number) && $card_number != '') {
			global $type;
			$cardtype = array(
				"visa" => "/^4[0-9]{12}(?:[0-9]{3})?$/",
				"mastercard" => "/^5[1-5][0-9]{14}$/",
				"amex" => "/^3[47][0-9]{13}$/",
				"jcb" => "/^(?:2131|1800|35\d{3})\d{11}$/",
				"dinnerclub" => "/^3(?:0[0-5]|[68][0-9])[0-9]{11}$/",
				"discover" => "/^6(?:011|5[0-9]{2})[0-9]{12}$/",
			);
			if (preg_match($cardtype['visa'], $card_number)) {
				return 'visa';
			} else if (preg_match($cardtype['mastercard'], $card_number)) {
				return 'mastercard';
			} else if (preg_match($cardtype['dinnerclub'], $card_number)) {
				return 'dinnerclub';
			} else if (preg_match($cardtype['jcb'], $card_number)) {
				return 'jcb';
			} else if (preg_match($cardtype['amex'], $card_number)) {
				return 'amex';
			} else if (preg_match($cardtype['discover'], $card_number)) {
				return 'discover';
			} else {
				return '';
			}
		}
	}
}
//======================== validate_customer_card ===================================

//======================== wallet ===================================
if (!function_exists('add_wallet')) {
	function add_wallet($user_id, $role = '') {
		$CI = &get_instance();		
		$CI->load->model('Wallet');
		$data['user_id'] = $user_id;
		$data['role'] = $role;
		$data['amount'] = 0;
		$data['create_date'] = date('Y-m-d H:i:s');			
		$wallet_id = Wallet::insertGetId($data);				
		if ($wallet_id != "" && $wallet_id > 0) {
			return $wallet_id;
		} else {
			return null;
		}
	}
}
//======================== wallet ===================================

//======================== check wallet balance ===================================
if (!function_exists('wallet_balance')) {
	function wallet_balance($user_id, $role = '') {
		$CI = &get_instance();
		$CI->load->model('Wallet');
		$amount = Wallet::whereUserId($user_id)->whereRole($role)->first();		
		return number_format($amount->amount, '2');
	}
}
//======================== check wallet balance ===================================

//======================== multi explode ===================================
if (!function_exists('multiexplode')) {
	function multiexplode($delimiters,$string) {		
	    $ready = str_replace($delimiters, $delimiters[0], $string);
	    $launch = explode($delimiters[0], $ready);
	    return  $launch;
	}
}
//======================== multi explode ===================================

//======================== check_time_slot ===================================
if (!function_exists('check_time_slot')) {
	function check_time_slot($data = []) {		
		$CI = &get_instance();
		$CI->load->model('Availability');
	    $availability_time = Availability::whereProviderId($data['provider_id'])->first()->toArray();		
		if (isset($availability_time) && $availability_time != "")
		{
			$slot_arr = multiexplode(array("|"),$availability_time['mon']);
		} 
		else 
		{
			$slot_arr = "";
		}
		$time_arr = [];
		$available_slot = [];
		foreach ($slot_arr as $row) {
			$time_arr = multiexplode(array("-"), $row);	
			if (strtotime($time_arr[0]) >= strtotime($data['start']) && strtotime($time_arr[0]) <= strtotime($data['end']) && strtotime($time_arr[1]) >= strtotime($data['start']) && strtotime($time_arr[1]) <= strtotime($data['end'])) 
			{
				return false;
			}
			else
			{
				$available_slot['start'] = $data['start'];
				$available_slot['end'] = $data['end'];
				$available_solt_arr = get_time($available_slot['start'], $available_slot['end']);
			}
		}				
		return $available_slot;
	}
}
//======================== check_time_slot ===================================

//======================== get_time ===================================
if (!function_exists('get_time')) {
	function get_time($start = '' , $end = '')
	{		
		if ($start !='' && $end !='') 
		{
			return $arr = $start . ':' . $end;							
		}

		if (!empty($arr)) 
		{
			return implode(",", $arr);
		}
	}
}
//======================== get_time ===================================

//======================== get_time ===================================
if (!function_exists('rating')) {
	function rating($user_id = '')
	{		
		$CI = &get_instance();
		$CI->load->model('rating');
		$rating = Rating::whereReceiverId($user_id)->avg('rating')->groupBy('receiver_id')->first();
		$rating = '';
		if (isset($rating) && !empty($rating)) 
		{
			return number_format($rating->rating, '2');
		} else
		{
			return number_format(5, '2');
		}
	}
}
//======================== get_time ===================================