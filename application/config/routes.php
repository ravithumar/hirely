<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*

| -------------------------------------------------------------------------

| URI ROUTING

| -------------------------------------------------------------------------

| This file lets you re-map URI requests to specific controller functions.

|

| Typically there is a one-to-one relationship between a URL string

| and its corresponding controller class/method. The segments in a

| URL normally follow this pattern:

|

|   example.com/class/method/id/

|

| In some instances, however, you may want to remap this relationship

| so that a different class/function is called than the one

| corresponding to the URL.

|

| Please see the user guide for complete details:

|

|   https://codeigniter.com/user_guide/general/routing.html

|

| -------------------------------------------------------------------------

| RESERVED ROUTES

| -------------------------------------------------------------------------

|

| There are three reserved routes:

|

|   $route['default_controller'] = 'welcome';

|

| This route indicates which controller class should be loaded if the

| URI contains no data. In the above example, the "welcome" class

| would be loaded.

|

|   $route['404_override'] = 'errors/page_missing';

|

| This route will tell the Router which controller/method to use if those

| provided in the URL cannot be matched to a valid route.

|

|   $route['translate_uri_dashes'] = FALSE;

|

| This is not exactly a route, but allows you to automatically route

| controller and method names that contain dashes. '-' isn't a valid

| class or method name character, so it requires translation.

| When you set this option to TRUE, it will replace ALL dashes in the

| controller and method URI segments.

|

| Examples: my-controller/index -> my_controller/index

|       my-controller/my-method -> my_controller/my_method

 */

$route['default_controller'] = 'HomeController';

$route['404_override'] = '';

$route['translate_uri_dashes'] = FALSE;

$route['admin/change/status/(:any)/(:num)'] = 'Admin/AdminController/change_status/$1/$2';

$route['admin/system/setting'] = 'Admin/SystemController/index';

$route['admin/app/setting'] = 'Admin/SystemController/app_setting';

$route['admin'] = 'Admin/DashboardController/index';

$route['check-duplicate-data'] = 'CommonController/check_duplicate_data';

// $route['admin/delete/(:any)'] = 'CommonController/delete/$1';

//website

$route['home'] = 'HomeController/index';

$route['services'] = 'HomeController/services';

$route['about-us'] = 'HomeController/about_us';

$route['contact-us'] = 'HomeController/contact_us';

$route['privacy-policy'] = 'HomeController/privacy_policy';

$route['terms-conditions'] = 'HomeController/terms_conditions';



// Category

$route['admin/category'] = 'Admin/CategoryController/index';

$route['admin/category/add'] = 'Admin/CategoryController/add';

$route['admin/category/edit/(:any)'] = 'Admin/CategoryController/edit/$1';

$route['admin/category/delete/(:any)'] = 'Admin/CategoryController/delete/$1';



// Sub Category

$route['admin/sub-category'] = 'Admin/SubcategoryController/index';

$route['admin/sub-category/add'] = 'Admin/SubcategoryController/add';

$route['admin/sub-category/edit/(:any)'] = 'Admin/SubcategoryController/edit/$1';

$route['admin/sub-category/delete/(:any)'] = 'Admin/SubcategoryController/delete/$1';



// Serveices

$route['admin/service'] = 'Admin/ServicesController/index';

$route['admin/service/add'] = 'Admin/ServicesController/add';

$route['admin/service/edit/(:any)'] = 'Admin/ServicesController/edit/$1';

$route['admin/service/delete/(:any)'] = 'Admin/ServicesController/delete/$1';

$route['admin/service/view/(:any)'] = 'Admin/ServicesController/view/$1';

$route['admin/service/remove-image'] = 'Admin/ServicesController/remove_image';


// Promocode

$route['admin/promocode'] = 'Admin/PromocodeController/index';

$route['admin/promocode/add'] = 'Admin/PromocodeController/add';

$route['admin/promocode/edit/(:any)'] = 'Admin/PromocodeController/edit/$1';

$route['admin/promocode/delete/(:any)'] = 'Admin/PromocodeController/delete/$1';

$route['admin/promocode/view/(:any)'] = 'Admin/PromocodeController/view/$1';



// Advertisement

$route['admin/advertisement'] = 'Admin/AdvertisementController/index';

$route['admin/advertisement/add'] = 'Admin/AdvertisementController/add';

$route['admin/advertisement/edit/(:any)'] = 'Admin/AdvertisementController/edit/$1';

$route['admin/advertisement/delete/(:any)'] = 'Admin/AdvertisementController/delete/$1';

$route['admin/advertisement/view/(:any)'] = 'Admin/AdvertisementController/view/$1';



// Claims Subjects

$route['admin/claim-subject'] = 'Admin/ClaimSubjectsController/index';

$route['admin/claim-subject/add'] = 'Admin/ClaimSubjectsController/add';

$route['admin/claim-subject/edit/(:any)'] = 'Admin/ClaimSubjectsController/edit/$1';

$route['admin/claim-subject/delete/(:any)'] = 'Admin/ClaimSubjectsController/delete/$1';


// Claims 

$route['admin/claim'] = 'Admin/ClaimController/index';

$route['admin/claim/delete/(:any)'] = 'Admin/ClaimController/delete/$1';

$route['admin/claim/view/(:any)'] = 'Admin/ClaimController/view/$1';


// Users

$route['admin/users'] = 'Admin/UserController/index';

$route['admin/users/add'] = 'Admin/UserController/add';

$route['admin/users/edit/(:any)'] = 'Admin/UserController/edit/$1';

$route['admin/users/delete/(:any)'] = 'Admin/UserController/delete/$1';

$route['admin/users/view/(:any)'] = 'Admin/UserController/view/$1';

// Provider

$route['admin/service-provider'] = 'Admin/ServiceProviderController/index';

$route['admin/service-provider/add'] = 'Admin/ServiceProviderController/add';

$route['admin/service-provider/edit/(:any)'] = 'Admin/ServiceProviderController/edit/$1';

$route['admin/service-provider/delete/(:any)'] = 'Admin/ServiceProviderController/delete/$1';

$route['admin/service-provider/view/(:any)'] = 'Admin/ServiceProviderController/view/$1';

// Provider

$route['admin/terms-conditions'] = 'Admin/SystemController/terms_conditions';

$route['admin/privacy-policy'] = 'Admin/SystemController/privacy_policy';
