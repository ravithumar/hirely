<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class HomeController extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('SystemConfig');		
	}

	public function index()
	{
		render('index');
	}

	public function services()
	{
		render('services');
	}

	public function about_us()
	{
		render('about');
	}

	public function contact_us()
	{
		render('contact-us');
	}

	public function privacy_policy()
	{	
		$data['privacy_policy'] = SystemConfig::wherePath('privacy_policy')->first();		
		render('privacy-policy', $data);
	}

	public function terms_conditions()
	{
		$data['terms_conditions'] = SystemConfig::wherePath('terms_conditions')->first();
		render('terms-and-conditions', $data);
	}
}
