<?php

defined('BASEPATH') or exit('No direct script access allowed');

class ClaimController extends MY_Controller

{

    function __construct()

    {

        parent::__construct();

        if (!$this->ion_auth->is_admin())

        {

            redirect('/');

        }

        $this->load->model('Claim');

        $this->load->model('User');

        $this->table_name = "claim";

        $this->title = "Claim";

    }

    public function index()

    {

        $data['title'] = $this->title;

        $this->load->library('Datatables');

        $product = new Datatables;

        $product->select('*', false)

            ->from($this->table_name);

        $action['edit'] = base_url('admin/claim/edit/');

        $action['view'] = base_url('admin/claim/view/');
        $action['delete'] = base_url('admin/claim/delete/');        

        $product->style(['class' => 'table table-striped table-bordered nowrap'])

            ->column('#', 'id')

            ->column('Reason', 'reason')            

            ->column('Approve', 'status', function ($status, $row)

	        {

	            if ($status == 0)

	            {

	                return '<h5 class="mb-0 mt-0"><span class="badge badge-warning cursor-pointer font-15 status_' . $row['id'] . '" data-table="' . $this->table_name . '" data-id="' . $row['id'] . '">Active</span></h5>';

	            }

	            else if($status == 1)

	            {

	                return '<h5 class="mb-0 mt-0"><span class="badge badge-info cursor-pointer font-15 status_' . $row['id'] . '" data-table="' . $this->table_name . '" data-id="' . $row['id'] . '">In Process</span></h5>';

	            }

	        })

            ->column('Actions', 'id', function ($id) use ($action)

            {

                $option = '<a href="' . $action['view'] . $id . '"  class="on-default text-green pr-1" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "View Record" data-rediret-url="' . current_url() . '"  data-table="' . $this->table_name . '" data-href="' . $this->table_name . '"><div class="btn-group btn-group-sm" style="float: none;"><button type="button" class="btn btn-info waves-effect waves-light" style="float: none;"><span class="dripicons-preview"></span></button></div></a>';

                $option .= '<a href="' . $action['edit'] . $id . '"><div class="btn-group btn-group-sm" style="float: none;"><button type="button" class="tabledit-edit-button btn btn-success" style="float: none;"><span class="mdi mdi-pencil"></span></button></div> </a>';

                $option .= '<a data-href="' . $action['delete'] . $id . '" href="javascript:void(0);" onclick="delete_confirm(this);"  class="on-default text-danger confirm_model" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "Delete Field" data-rediret-url="' . current_url() . '"  data-table="' . $this->table_name . '" ><div class="btn-group btn-group-sm" style="float: none;"><button type="button" class="btn btn-danger waves-effect waves-light" style="float: none;"><span class="mdi mdi-close"></span></button></div></a>';

                return $option;

            });

	     

        $product->datatable($this->table_name);

        $product->init();

        $data['datatable'] = true;

        $data['export'] = false;

        $data['add_url'] = base_url('admin/claim/add');        

        $data['title'] = $this->title;

        $data['main_title'] = $this->title;

        $this->renderAdmin('claim/index', $data);

    }



    public function view($id) 

    {   

        $data['title'] = "Claim View";

        $data['table'] = "claim";

        $data['home'] = base_url('admin/claim');

        $data['main_title'] = $this->title;  

        $data['claim'] = Claim::find($id);         

        $this->renderAdmin('claim/view', $data);

    }

    public function delete($id) 

    {        

        $post['deleted_at'] = current_date();

        $deleted_record = Claim::where('id', $id)->update($post);

        if (isset($deleted_record) && $deleted_record > 0) {

            $json_data = 1;

        } else {

            $json_data = 0;

        }

        echo json_encode($json_data);

    }

}



