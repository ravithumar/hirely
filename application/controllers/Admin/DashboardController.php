<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class DashboardController extends MY_Controller {
	function __construct() {
		parent::__construct();
		if (!$this->ion_auth->is_admin()) {
			redirect('/');
		}
		$this->load->model('advertisement');
		$this->load->model('category');
		$this->load->model('service');
		$this->load->model('user');

	}
	public function index() {

		$data['customer'] = User::count();
		$data['service_provider'] = 0;
		$data['inprogress'] = 0;
		$data['confirmed'] = 0;
		$data['canceled'] = 0;
		$data['finished_order'] = 0;

		$this->renderAdmin('dashboard', $data);
	}
}