<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class SystemController extends MY_Controller {
	function __construct() {
		parent::__construct();
		if(!$this->ion_auth->is_admin())
		{
			redirect('/');
		}
		$this->load->model('SystemConfig');
		
	}
	public function index() {
        $this->load->helper('directory');
        $data['backup'] = directory_map('assets/DB/');
        $this->load->library('breadcrumbs');
		$this->breadcrumbs->push('Dashboard', '/admin');
		$this->breadcrumbs->push('System Configuration', 'admin/system/setting');
		$this->renderAdmin('system_config/index',$data);
	}
	public function save_system_config() {
		$request = $this->input->post();
		$request['shipper_individual_account'] = isset($request['shipper_individual_account'])?1:0;
		$request['shipper_company_account'] = isset($request['shipper_company_account'])?1:0;
		$request['carrier_individual_account'] = isset($request['carrier_individual_account'])?1:0;
		$request['carrier_company_account'] = isset($request['carrier_company_account'])?1:0;
		foreach ($request as $key => $value) {
			$key_array[$key] = $value;
		}
		if ($_FILES['site_logo']['name'] != "") {
			$image_upload_resp = image_upload('site_logo', 'images/logo');
			if ($image_upload_resp['status']) {
				$key_array['site_logo'] = $image_upload_resp['file_name'];
			} else {
				$this->session->set_flashdata('danger', $image_upload_resp['error']);
			}
		}
		config_set($key_array);
		$this->session->set_flashdata('success', __('System Configuration details updated successfully.'));
		redirect('admin/system/setting');
	}
	public function app_setting() {
		$data['app'] = $this->db->get('app_version')->result();
		$this->load->library('breadcrumbs');
		$this->breadcrumbs->push('Dashboard', '/admin');
		$this->breadcrumbs->push('App Configuration', 'admin/app/setting');
		$this->renderAdmin('system_config/app_setting', $data);
	}
	public function save_app_config() {
		$request = $this->input->post();
		$android_customer['version']=$request['android_customer_version'];
		$android_customer['force_update']=isset($request['android_customer_update'])?1:0;
		$android_customer['maintenance']=isset($request['customer_maintenance'])?1:0;
		// _pre($android_customer);
		$this->db->where('type','android_customer')->update('app_version',$android_customer);
		$ios_customer['version']=$request['ios_customer_version'];
		$ios_customer['force_update']=isset($request['ios_customer_update'])?1:0;
		$ios_customer['maintenance']=isset($request['customer_maintenance'])?1:0;
		$this->db->where('type','ios_customer')->update('app_version',$ios_customer);
		$android_driver['version']=$request['android_driver_version'];
		$android_driver['force_update']=isset($request['android_driver_update'])?1:0;
		$android_driver['maintenance']=isset($request['driver_maintenance'])?1:0;
		$this->db->where('type','android_driver')->update('app_version',$android_driver);
		$ios_driver['version']=$request['ios_driver_version'];
		$ios_driver['force_update']=isset($request['ios_driver_update'])?1:0;
		$ios_driver['maintenance']=isset($request['driver_maintenance'])?1:0;
		$this->db->where('type','ios_driver')->update('app_version',$ios_driver);
		$this->session->set_flashdata('success', __('App Configuration details updated successfully.'));
		redirect('admin/app/setting');
	}
	public function backup() {
		$this->load->dbutil();
		$filename = time() . '.zip';
		$prefs = array(
			'ignore' => array(), // List of tables to omit from the backup
			'format' => 'zip', // gzip, zip, txt
			'filename' => $filename, // File name - NEEDED ONLY WITH ZIP FILES
			'add_drop' => TRUE, // Whether to add DROP TABLE statements to backup file
			'add_insert' => TRUE, // Whether to add INSERT data to backup file
			'newline' => "\n", // Newline character used in backup file
		);
		$backup = $this->dbutil->backup($prefs);
		$this->load->helper('file');
		write_file('assets/DB/' . $filename, $backup);
		// $this->load->helper('download');
		// force_download($filename, $backup);
        $this->session->set_flashdata('success', __('Database backup successfully.'));
        redirect('admin/system/setting');
	}
    public function download($filename="")
    {
        if($filename !="")
        {
            $this->load->helper('download');
            $data = file_get_contents("assets/DB/".$filename); // Read the file's contents
            $name = $filename;
            force_download($name, $data);
            $this->session->set_flashdata('success', __('Backup download successfully.'));
            redirect('admin/system/setting');
        }
        else
        {
            $this->session->set_flashdata('error', __('Somthing went wrong due to download backup!'));
            redirect('admin/system/setting');   
        }
    }
    public function delete_backup($filename="")
    {
        if($filename !="")
        {
           unlink('assets/DB/'.$filename);
           $this->session->set_flashdata('success', __('Backup deleted successfully.'));
            redirect('admin/system/setting');
        }
        else
        {
            $this->session->set_flashdata('error', __('Somthing went wrong due to delete backup!'));
            redirect('admin/system/setting');   
        }
    }

    public function terms_conditions() {		

		$data['title'] = "Terms And Conditions Update";

		$data['main_title'] = "Terms And Conditions";

		$data['home'] = base_url('admin/terms-conditions');
		$data['action_path'] = base_url('admin/terms-conditions');
		$data['cms_pages'] = SystemConfig::wherePath('terms_conditions')->first();		
		if (isset($_POST) && !empty($_POST)) {

			$request = $this->input->post();				

			$post['value'] = $request['description'];

			SystemConfig::whereId($request['id'])->update($post);

			$this->session->set_flashdata('success', __('Terms And Conditions Update successfully'));

			redirect('admin/terms-conditions');

		}		

		$this->renderAdmin('system_config/cms_pages', $data);

	}

	public function privacy_policy() {		

		$data['title'] = "Privacy Policy Update";

		$data['main_title'] = "Privacy Policy";

		$data['home'] = base_url('admin/privacy-policy');
		$data['action_path'] = base_url('admin/privacy-policy');
		$data['cms_pages'] = SystemConfig::wherePath('privacy_policy')->first();
		if (isset($_POST) && !empty($_POST)) {

			$request = $this->input->post();						

			$post['value'] = $request['description'];

			SystemConfig::whereId($request['id'])->update($post);

			$this->session->set_flashdata('success', __('Privacy Policy Update successfully'));

			redirect('admin/privacy-policy');

		}		

		$this->renderAdmin('system_config/cms_pages', $data);

	}
}