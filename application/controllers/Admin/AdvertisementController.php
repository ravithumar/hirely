<?php

defined('BASEPATH') or exit('No direct script access allowed');

class AdvertisementController extends MY_Controller

{

    function __construct()

    {

        parent::__construct();

        if (!$this->ion_auth->is_admin())

        {

            redirect('/');

        }

        $this->load->model('advertisement');        

        $this->table_name = "advertisement";

        $this->title = "Advertisement";

    }

    public function index()

    {

        $data['title'] = $this->title;

        $this->load->library('Datatables');

        $product = new Datatables;

        $product->select('*', false)

            ->from($this->table_name)            

            ->where('deleted_at', NULL);            

        $action['edit'] = base_url('admin/advertisement/edit/');

        $action['delete'] = base_url('admin/advertisement/delete/');

        $action['view'] = base_url('admin/advertisement/view/');

        $product->style(['class' => 'table table-striped table-bordered nowrap'])

            ->column('#', 'id')

            ->column('Title', 'title')

            ->column(__('Description') , 'description', function ($status, $row)
            {

                $material_editer = strip_tags($row['description']);
                $str_title = strlen($material_editer);
                // print_r($str_title);exit();
                if ($str_title > 48)
                {
                    $title = substr($material_editer, 0, 48) . "...";
                }
                else
                {
                    $title = $material_editer;
                }
                return $title;
            })

            ->column('Approve', 'status', function ($status, $row)

            {

                if ($status == 1)

                {

                    return '<h5 class="mb-0 mt-0"><span onclick="fun_change_state(this);" class="badge badge-success cursor-pointer font-15 status_' . $row['id'] . '" data-table="' . $this->table_name . '" data-id="' . $row['id'] . '">Active</span></h5>';

                }

                else

                {

                    return '<h5 class="mb-0 mt-0"><span onclick="fun_change_state(this);" class="badge badge-danger cursor-pointer font-15 status_' . $row['id'] . '" data-table="' . $this->table_name . '" data-id="' . $row['id'] . '">Inactive</span></h5>';

                }

            })

            ->column('Actions', 'id', function ($id) use ($action)

            {

                $option = '<a href="' . $action['view'] . $id . '"  class="on-default text-green pr-1" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "View Record" data-rediret-url="' . current_url() . '"  data-table="' . $this->table_name . '" data-href="' . $this->table_name . '"><div class="btn-group btn-group-sm" style="float: none;"><button type="button" class="btn btn-info waves-effect waves-light" style="float: none;"><span class="dripicons-preview"></span></button></div></a>';

                $option .= '<a href="' . $action['edit'] . $id . '"><div class="btn-group btn-group-sm" style="float: none;"><button type="button" class="tabledit-edit-button btn btn-success" style="float: none;"><span class="mdi mdi-pencil"></span></button></div> </a>';

                $option .= '<a data-href="' . $action['delete'] . $id . '" href="javascript:void(0);" onclick="delete_confirm(this);"  class="on-default text-danger confirm_model" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "Delete Field" data-rediret-url="' . current_url() . '"  data-table="' . $this->table_name . '" ><div class="btn-group btn-group-sm" style="float: none;"><button type="button" class="btn btn-danger waves-effect waves-light" style="float: none;"><span class="mdi mdi-close"></span></button></div></a>';

                return $option;

            });


        $product->datatable($this->table_name);

        $product->init();

        $data['datatable'] = true;

        $data['export'] = false;

        $data['add_url'] = base_url('admin/advertisement/add');

        $data['title'] = $this->title;        

        $data['main_title'] = $this->title;

        $this->renderAdmin('advertisement/index', $data);

    }

    public function add()

    {

        $data['title'] = "Advertisement Add";      

        $data['home'] = base_url('admin/advertisement');

        $data['main_title'] = $this->title;  

        if (isset($_POST) && !empty($_POST))

        {

            $request = $this->input->post();

            $post['title'] = $request['title'];            

            $post['description'] = $request['description'];

            $post['redirect_url'] = $request['redirect_url'];

            if (file_exists($_FILES['image']['tmp_name']) || is_uploaded_file($_FILES['image']['tmp_name']))

            {

                $image_is_uploaded = image_upload('image', 'images/advertisement/', true, 'jpg|JPG|png|PNG|jpeg|JPEG');

                if (isset($image_is_uploaded['status']) && $image_is_uploaded['status'])

                {

                    $post['image'] = $image_is_uploaded['uploaded_path'];

                }

            }

            Advertisement::insert($post);

            $this->session->set_flashdata('success', __('Advertisement Add successfully'));

            redirect('admin/advertisement');

        }

        $this->renderAdmin('advertisement/add', $data);

    }



    public function edit($id = '')

    {

        $data['advertisement'] = Advertisement::find($id);  

        $data['home'] = base_url('admin/advertisement');

        $data['main_title'] = $this->title;        

        $data['title'] = "Advertisement Edit";

        if (isset($_POST) && !empty($_POST))

        {

            $request = $this->input->post();

            $post['title'] = $request['title'];

            $post['description'] = $request['description'];

            $post['redirect_url'] = $request['redirect_url'];

            if (file_exists($_FILES['image']['tmp_name']) || is_uploaded_file($_FILES['image']['tmp_name']))

            {

                $image_is_uploaded = image_upload('image', 'images/advertisement/', true, 'jpg|JPG|png|PNG|jpeg|JPEG');

                if (isset($image_is_uploaded['status']) && $image_is_uploaded['status'])

                {

                    $post['image'] = $image_is_uploaded['uploaded_path'];

                }

            }

            Advertisement::where('id', $id)->update($post);

            $this->session->set_flashdata('success', __('Advertisement Update successfully'));

            redirect('admin/advertisement');

        }

        $this->renderAdmin('advertisement/edit', $data);

    }



    public function view($id) 

    {   

        $data['title'] = "Advertisement View";

        $data['home'] = base_url('admin/advertisement');

        $data['main_title'] = $this->title;  

        $data['advertisement'] = Advertisement::find($id);        

        $this->renderAdmin('advertisement/view', $data);   

    }



    public function delete($id) 

    {        

        $post['deleted_at'] = current_date();

        $deleted_record = Advertisement::where('id', $id)->update($post);

        if (isset($deleted_record) && $deleted_record > 0) {

            $json_data = 1;

        } else {

            $json_data = 0;

        }

        echo json_encode($json_data);

    }

}



