<?php

defined('BASEPATH') or exit('No direct script access allowed');

class SubcategoryController extends MY_Controller {

	function __construct() {

		parent::__construct();

		if (!$this->ion_auth->is_admin()) {

			redirect('/');

		}

		$this->load->model('SubCategory');

		$this->load->model('Category');

		$this->table_name = "sub_category";

		$this->title = "Sub Category";

	}

	public function index() {

		$data['title'] = $this->title;

		$this->load->library('Datatables');

		$product = new Datatables;

		$product->select('category.name as category_name, sub_category.id, sub_category.name, sub_category.status', false)

			->from($this->table_name)

			->join('category', 'category.id = sub_category.category_id');

		$action['edit'] = base_url('admin/sub-category/edit/');
		$action['delete'] = base_url('admin/sub-category/delete/');
		$action['view'] = base_url('admin/sub-category/view/');

		$product->style(['class' => 'table table-striped table-bordered nowrap'])

			->column('#', 'id')

			->column('Name', 'name')

			->column('Category Name', 'category_name')->column('Approve', 'status', function ($status, $row) {

			if ($status == 1) {

				return '<h5 class="mb-0 mt-0"><span onclick="fun_change_state(this);" class="badge badge-success cursor-pointer font-15 status_' . $row['id'] . '" data-table="' . $this->table_name . '" data-id="' . $row['id'] . '">Active</span></h5>';

			} else {

				return '<h5 class="mb-0 mt-0"><span onclick="fun_change_state(this);" class="badge badge-danger cursor-pointer font-15 status_' . $row['id'] . '" data-table="' . $this->table_name . '" data-id="' . $row['id'] . '">Inactive</span></h5>';

			}

		})

			->column('Actions', 'id', function ($id) use ($action) {

				$option = '<a href="' . $action['edit'] . $id . '"><div class="btn-group btn-group-sm" style="float: none;"><button type="button" class="tabledit-edit-button btn btn-success" style="float: none;"><span class="mdi mdi-pencil"></span></button></div> </a>';

				$option .= '<a data-href="' . $action['delete'] . $id . '" href="javascript:void(0);" onclick="delete_confirm(this);"  class="on-default text-danger confirm_model" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "Delete Field" data-rediret-url="' . current_url() . '"  data-table="' . $this->table_name . '" ><div class="btn-group btn-group-sm" style="float: none;"><button type="button" class="btn btn-danger waves-effect waves-light" style="float: none;"><span class="mdi mdi-close"></span></button></div></a>';

				return $option;

			});

		$product->datatable($this->table_name);

		$product->init();

		$data['datatable'] = true;

		$data['export'] = false;

		$data['add_url'] = base_url('admin/sub-category/add');

		$data['title'] = $this->title;

		$data['main_title'] = $this->title;

		$this->renderAdmin('sub_category/index', $data);

	}

	public function add() {

		$data['title'] = "Sub Category Add";

		$data['main_title'] = $this->title;

		$data['category'] = Category::whereStatus('1')->whereDeletedAt(NULL)->get();

		$data['home'] = base_url('admin/sub-category');

		if (isset($_POST) && !empty($_POST)) {

			$request = $this->input->post();

			$post['name'] = $request['name'];

			$post['category_id'] = $request['category_id'];

			SubCategory::insert($post);

			$this->session->set_flashdata('success', __('Sub Category Add successfully'));

			redirect('admin/sub-category');

		}

		$this->renderAdmin('sub_category/add', $data);

	}

	public function edit($id = '') {

		$data['sub_category'] = SubCategory::find($id);

		$data['category'] = Category::whereStatus('1')->whereDeletedAt(NULL)->get();

		$data['title'] = "Sub Category Edit";

		$data['home'] = base_url('admin/sub-category');

		$data['main_title'] = $this->title;

		if (isset($_POST) && !empty($_POST)) {

			$request = $this->input->post();

			$post['name'] = $request['name'];

			$post['category_id'] = $request['category_id'];

			SubCategory::where('id', $id)->update($post);

			$this->session->set_flashdata('success', __('Sub Category Update successfully'));

			redirect('admin/sub-category');

		}

		$this->renderAdmin('sub_category/edit', $data);

	}

	public function delete($id) {

		$post['deleted_at'] = current_date();

		$deleted_record = SubCategory::where('id', $id)->update($post);

		if (isset($deleted_record) && $deleted_record > 0) {

			$json_data = 1;

		} else {

			$json_data = 0;

		}

		echo json_encode($json_data);

	}

}
