<?php

defined('BASEPATH') or exit('No direct script access allowed');

class ServicesController extends MY_Controller

{

    function __construct()

    {

        parent::__construct();

        if (!$this->ion_auth->is_admin())

        {

            redirect('/');

        }

        $this->load->model('Service');

        $this->load->model('User');

        $this->load->model('ServiceImage');

        $this->load->model('SubCategory');

        $this->load->model('Category');

        $this->table_name = "services";

        $this->title = "Service";

    }

    public function index()

    {

        $data['title'] = $this->title;

        $this->load->library('Datatables');

        $product = new Datatables;

        $product->select('services.title, services.booking_price, services.fixed_price, services.status, services.id, category.name as category_name, sub_category.name as sub_category_name', false)

            ->from($this->table_name)

            ->join('users', 'users.id = services.user_id','left')

            ->join('category', 'category.id = services.category_id','left')

            ->join('sub_category', 'sub_category.id = services.sub_category_id','left');

        $action['edit'] = base_url('admin/service/edit/');
        $action['delete'] = base_url('admin/service/delete/');
        $action['view'] = base_url('admin/service/view/');

        $product->style(['class' => 'table table-striped table-bordered nowrap'])

            ->column('#', 'id')

            ->column('Service Name ', 'title')

            ->column('Category ', 'category_name')

            ->column('Sub Category', 'sub_category_name')

            ->column('Booking Price', 'booking_price')

            ->column('Fixed Price', 'fixed_price')

            ->column('Approve', 'status', function ($status, $row)

            {

                if ($status == 1)

                {

                    return '<h5 class="mb-0 mt-0"><span onclick="fun_change_state(this);" class="badge badge-success cursor-pointer font-15 status_' . $row['id'] . '" data-table="' . $this->table_name . '" data-id="' . $row['id'] . '">Active</span></h5>';

                }

                else

                {

                    return '<h5 class="mb-0 mt-0"><span onclick="fun_change_state(this);" class="badge badge-danger cursor-pointer font-15 status_' . $row['id'] . '" data-table="' . $this->table_name . '" data-id="' . $row['id'] . '">Inactive</span></h5>';

                }

            })

            ->column('Actions', 'id', function ($id) use ($action)

            {

                $option = '<a href="' . $action['view'] . $id . '"  class="on-default text-green pr-1" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "View Record" data-rediret-url="' . current_url() . '"  data-table="' . $this->table_name . '" data-href="' . $this->table_name . '"><div class="btn-group btn-group-sm" style="float: none;"><button type="button" class="btn btn-info waves-effect waves-light" style="float: none;"><span class="dripicons-preview"></span></button></div></a>';

                $option .= '<a href="' . $action['edit'] . $id . '"><div class="btn-group btn-group-sm" style="float: none;"><button type="button" class="tabledit-edit-button btn btn-success" style="float: none;"><span class="mdi mdi-pencil"></span></button></div> </a>';

                $option .= '<a data-href="' . $action['delete'] . $id . '" href="javascript:void(0);" onclick="delete_confirm(this);"  class="on-default text-danger confirm_model" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "Delete Field" data-rediret-url="' . current_url() . '"  data-table="' . $this->table_name . '" ><div class="btn-group btn-group-sm" style="float: none;"><button type="button" class="btn btn-danger waves-effect waves-light" style="float: none;"><span class="mdi mdi-close"></span></button></div></a>';

                return $option;

            });



        $product->datatable($this->table_name);

        $product->init();

        $data['datatable'] = true;

        $data['export'] = false;

        $data['add_url'] = base_url('admin/service/add');

        $data['title'] = $this->title;

        $data['main_title'] = $this->title;

        $this->renderAdmin('service/index', $data);

    }

    public function add()

    {

        $data['title'] = "Service Add";

        $data['main_title'] = $this->title;

        $data['home'] = base_url('admin/service');

        $data['provider'] = User::all();

        if (isset($_POST) && !empty($_POST))

        {

            $request = $this->input->post();

            $post['user_id'] = $request['provider_id'];

            $post['booking_price'] = $request['booking_price'];

            $post['fixed_price'] = $request['fixed_price'];

            $last_insert_id = Service::insert($post);

            if (file_exists($_FILES['image']['tmp_name']) || is_uploaded_file($_FILES['image']['tmp_name']))

            {

                $image_is_uploaded = image_upload('image', 'images/category/', true, 'jpg|JPG|png|PNG|jpeg|JPEG');

                if (isset($image_is_uploaded['status']) && $image_is_uploaded['status'])

                {

                    $add_more['image'] = $image_is_uploaded['uploaded_path'];

                }

            }

            $add_more['service_id'] = $last_insert_id;

            $add_more['created_at'] = date('Y-m-d');

            $service_image_id = ServiceImage::insert($add_more);

            $this->session->set_flashdata('success', __('Service Add successfully'));

            redirect('admin/service');

        }

        $this->renderAdmin('service/add', $data);

    }



    public function edit($id = '')

    {

        $data['service'] = Service::find($id);

        $data['service_image'] = ServiceImage::whereServiceId($id)->get();

        $data['users'] = User::whereId($data['service']->user_id)->first();        

        $data['provider'] = User::all();

        $data['category'] = Category::whereStatus('1')->get();

        $data['sub_category'] = SubCategory::whereStatus('1')->get();

        $data['title'] = "Service Edit";

        $data['home'] = base_url('admin/service');

        $data['main_title'] = $this->title;

        if (isset($_POST) && !empty($_POST))

        {

            $request = $this->input->post();

            $post['user_id'] = $request['provider_id'];

            $post['booking_price'] = $request['booking_price'];

            $post['fixed_price'] = $request['fixed_price'];

            $post['title'] = $request['title'];

            $post['description'] = $request['description'];

            $last_insert_id = Service::where('id', $id)->update($post);

                        

            if (isset($_FILES['image']['name']) && $_FILES['image']['name'] != '') 

            {

                $image_data = array($_FILES['image']);

                if ($image_data[0]['name'] != "") 

                {

                    $all_images = array();                      

                    $files = $_FILES;

                    $count = count($_FILES['image']['name']);                  

                    for ($i = 0; $i < $count; $i++) 

                    {

                        $_FILES['image']['name'] = $files['image']['name'][$i];

                        $_FILES['image']['type'] = $files['image']['type'][$i];

                        $_FILES['image']['tmp_name'] = $files['image']['tmp_name'][$i];

                        $_FILES['image']['error'] = $files['image']['error'][$i];

                        $_FILES['image']['size'] = $files['image']['size'][$i];

                        $image_is_uploaded = image_upload('image', 'images/service/', true, 'jpg|JPG|png|PNG|jpeg|JPEG');

                        $img_array = [];

                        if (isset($image_is_uploaded['status']) && $image_is_uploaded['status'])

                        {

                            $all_images[] = $image_is_uploaded['uploaded_path']; 

                        }

                    }

                    foreach ($all_images as $key => $value) 

                    {

                        $add_more['image'] = $value;

                        $add_more['service_id'] = $id;                        

                        $service_image_id = ServiceImage::insert($add_more);

                    }

                }

            }      

            $this->session->set_flashdata('success', __('Service Update successfully'));

            redirect('admin/service');

        }

        $this->renderAdmin('service/edit', $data);

    }



    public function remove_image() {

        $post = $this->input->post();        

        if (!empty($post)) {

            $id = $post['img_id'];

            $this->db->where('id', $id);

            $this->db->delete('service_images');

            echo json_encode(array('is_success' => true));

        } else {

            echo json_encode(array('is_success' => false));

        }

    }



    public function profile() {

        $this->renderAdmin('provider/profile');   

    }

    public function delete($id) 

    {        

        $post['deleted_at'] = current_date();

        $deleted_record = Service::where('id', $id)->update($post);

        if (isset($deleted_record) && $deleted_record > 0) {

            $json_data = 1;

        } else {

            $json_data = 0;

        }

        echo json_encode($json_data);

    }

}



