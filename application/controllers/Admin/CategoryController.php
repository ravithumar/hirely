<?php

defined('BASEPATH') or exit('No direct script access allowed');

class CategoryController extends MY_Controller {

	function __construct() {

		parent::__construct();

		if (!$this->ion_auth->is_admin()) {

			redirect('/');

		}

		$this->load->model('category');

		$this->table_name = "category";

		$this->title = "Category";

	}

	public function index() {

		$data['title'] = $this->title;

		$this->load->library('Datatables');

		$product = new Datatables;

		$product->select('*', false)

			->from($this->table_name);

		$action['edit'] = base_url('admin/category/edit/');
		$action['delete'] = base_url('admin/category/delete/');
		$action['view'] = base_url('admin/category/view/');

		$product->style(['class' => 'table table-striped table-bordered nowrap'])

			->column('#', 'id')

			->column('Name', 'name')->column('Image', 'image', function ($image_url) {

			if ($image_url != '' && isset($image_url)) {

				$option = '<img src="' . base_url() . $image_url . '" name="product_image" class="image_size">';

			} else {

				$image_url = "assets/images/default-profile.png";

				$option = '<img src="' . base_url() . $image_url . '" name="product_image" class="image_size">';

			}

			return $option;

		})

			->column('Approve', 'status', function ($status, $row) {

				if ($status == 1) {

					return '<h5 class="mb-0 mt-0"><span onclick="fun_change_state(this);" class="badge badge-success cursor-pointer font-15 status_' . $row['id'] . '" data-table="' . $this->table_name . '" data-id="' . $row['id'] . '">Active</span></h5>';

				} else {

					return '<h5 class="mb-0 mt-0"><span onclick="fun_change_state(this);" class="badge badge-danger cursor-pointer font-15 status_' . $row['id'] . '" data-table="' . $this->table_name . '" data-id="' . $row['id'] . '">Inactive</span></h5>';

				}

			})

			->column('Actions', 'id', function ($id) use ($action) {

				$option = '<a href="' . $action['edit'] . $id . '"><div class="btn-group btn-group-sm" style="float: none;"><button type="button" class="tabledit-edit-button btn btn-success" style="float: none;"><span class="mdi mdi-pencil"></span></button></div> </a>';

				$option .= '<a data-href="' . $action['delete'] . $id . '" href="javascript:void(0);" onclick="delete_confirm(this);"  class="on-default text-danger confirm_model" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "Delete Field" data-rediret-url="' . current_url() . '"  data-table="' . $this->table_name . '" ><div class="btn-group btn-group-sm" style="float: none;"><button type="button" class="btn btn-danger waves-effect waves-light" style="float: none;"><span class="mdi mdi-close"></span></button></div></a>';

				return $option;

			});

		$product->datatable($this->table_name);

		$product->init();

		$data['datatable'] = true;

		$data['export'] = false;

		$data['add_url'] = base_url('admin/category/add');

		$data['title'] = $this->title;

		$data['main_title'] = $this->title;

		$this->renderAdmin('category/index', $data);

	}

	public function add() {		

		$data['title'] = "Category Add";

		$data['main_title'] = $this->title;

		$data['home'] = base_url('admin/category');

		if (isset($_POST) && !empty($_POST)) {

			$request = $this->input->post();

			$post['name'] = $request['name'];

			if (file_exists($_FILES['image']['tmp_name']) || is_uploaded_file($_FILES['image']['tmp_name'])) {

				$image_is_uploaded = image_upload('image', 'images/category/', true, 'jpg|JPG|png|PNG|jpeg|JPEG');

				if (isset($image_is_uploaded['status']) && $image_is_uploaded['status']) {

					$post['image'] = $image_is_uploaded['uploaded_path'];

				}

			}

			Category::insert($post);

			$this->session->set_flashdata('success', __('Category Add successfully'));

			redirect('admin/category');

		}

		$this->renderAdmin('category/add', $data);

	}

	public function edit($id = '') {

		$data['category'] = Category::find($id);

		$data['home'] = base_url('admin/category');

		$data['title'] = "Category Edit";

		$data['main_title'] = $this->title;

		if (isset($_POST) && !empty($_POST)) {

			$request = $this->input->post();

			$post['name'] = $request['name'];

			if (file_exists($_FILES['image']['tmp_name']) || is_uploaded_file($_FILES['image']['tmp_name'])) {

				$image_is_uploaded = image_upload('image', 'images/category/', true, 'jpg|JPG|png|PNG|jpeg|JPEG');

				if (isset($image_is_uploaded['status']) && $image_is_uploaded['status']) {

					$post['image'] = $image_is_uploaded['uploaded_path'];

				}

			}

			Category::where('id', $id)->update($post);

			$this->session->set_flashdata('success', __('Category Update successfully'));

			redirect('admin/category');

		}

		$this->renderAdmin('category/edit', $data);

	}

	public function delete($id) {

		$post['deleted_at'] = current_date();

		$deleted_record = Category::where('id', $id)->update($post);

		if (isset($deleted_record) && $deleted_record > 0) {

			$json_data = 1;

		} else {

			$json_data = 0;

		}

		echo json_encode($json_data);

	}

}
