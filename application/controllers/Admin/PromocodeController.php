<?php

defined('BASEPATH') or exit('No direct script access allowed');

class PromocodeController extends MY_Controller

{

    function __construct()

    {

        parent::__construct();

        if (!$this->ion_auth->is_admin())

        {

            redirect('/');

        }

        $this->load->model('promocode');

        $this->table_name = "promocode";

        $this->title = "Promocode";

    }

    public function index()

    {

        $data['title'] = $this->title;

        $this->load->library('Datatables');

        $product = new Datatables;

        $product->select('*', false)

            ->from($this->table_name);

        $action['edit'] = base_url('admin/promocode/edit/');
        $action['delete'] = base_url('admin/promocode/delete/');
        $action['view'] = base_url('admin/promocode/view/');

        $product->style(['class' => 'table table-striped table-bordered nowrap'])

            ->column('#', 'id')

            ->column('Code', 'code')

            ->column('Start Date', 'start_date')

            ->column('End Date', 'end_date')->column('Approve', 'status', function ($status, $row)

            {

                if ($status == 1)

                {

                    return '<h5 class="mb-0 mt-0"><span onclick="fun_change_state(this);" class="badge badge-success cursor-pointer font-15 status_' . $row['id'] . '" data-table="' . $this->table_name . '" data-id="' . $row['id'] . '">Active</span></h5>';

                }

                else

                {

                    return '<h5 class="mb-0 mt-0"><span onclick="fun_change_state(this);" class="badge badge-danger cursor-pointer font-15 status_' . $row['id'] . '" data-table="' . $this->table_name . '" data-id="' . $row['id'] . '">Inactive</span></h5>';

                }

            })

            ->column('Actions', 'id', function ($id) use ($action)

            {

                $option = '<a href="' . $action['view'] . $id . '"  class="on-default text-green pr-1" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "View Record" data-rediret-url="' . current_url() . '"  data-table="' . $this->table_name . '" data-href="' . $this->table_name . '"><div class="btn-group btn-group-sm" style="float: none;"><button type="button" class="btn btn-info waves-effect waves-light" style="float: none;"><span class="dripicons-preview"></span></button></div></a>';

                $option .= '<a href="' . $action['edit'] . $id . '"><div class="btn-group btn-group-sm" style="float: none;"><button type="button" class="tabledit-edit-button btn btn-success" style="float: none;"><span class="mdi mdi-pencil"></span></button></div> </a>';

                $option .= '<a data-href="' . $action['delete'] . $id . '" href="javascript:void(0);" onclick="delete_confirm(this);"  class="on-default text-danger confirm_model" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "Delete Field" data-rediret-url="' . current_url() . '"  data-table="' . $this->table_name . '" ><div class="btn-group btn-group-sm" style="float: none;"><button type="button" class="btn btn-danger waves-effect waves-light" style="float: none;"><span class="mdi mdi-close"></span></button></div></a>';

                return $option;

            });



        $product->datatable($this->table_name);

        $product->init();

        $data['datatable'] = true;

        $data['export'] = false;

        $data['add_url'] = base_url('admin/promocode/add');

        $data['title'] = $this->title;        

        $data['main_title'] = $this->title;

        $this->renderAdmin('promocode/index', $data);

    }

    public function add()

    {

        $data['title'] = "Promocode Add";

        $data['home'] = base_url('admin/promocode');

        $data['main_title'] = $this->title;

        if (isset($_POST) && !empty($_POST))

        {

            $request = $this->input->post();

            $post['code'] = $request['code'];

            $post['start_date'] = $request['start_date'];

            $post['end_date'] = $request['end_date'];
            $post['discount_type'] = $request['discount_type'];
            $post['discount_value'] = $request['discount_value'];
            $post['usage_limit'] = $request['usage_limit'];
            $post['description'] = $request['description'];
            if (file_exists($_FILES['image']['tmp_name']) || is_uploaded_file($_FILES['image']['tmp_name'])) {

                $image_is_uploaded = image_upload('image', 'images/promocode/', true, 'jpg|JPG|png|PNG|jpeg|JPEG');

                if (isset($image_is_uploaded['status']) && $image_is_uploaded['status']) {

                    $post['image'] = $image_is_uploaded['uploaded_path'];

                }

            }

            Promocode::insert($post);

            $this->session->set_flashdata('success', __('Promocode Add successfully'));

            redirect('admin/promocode');

        }

        $this->renderAdmin('promocode/add', $data);

    }



    public function edit($id = '')

    {

        $data['promocode'] = Promocode::find($id);

        $data['title'] = "Promocode Edit";

        $data['home'] = base_url('admin/promocode');

        $data['main_title'] = $this->title;

        if (isset($_POST) && !empty($_POST))

        {

            $request = $this->input->post();

            $post['code'] = $request['code'];

            $post['start_date'] = $request['start_date'];

            $post['end_date'] = $request['end_date'];
            $post['discount_type'] = $request['discount_type'];
            $post['discount_value'] = $request['discount_value'];
            $post['usage_limit'] = $request['usage_limit'];
            $post['description'] = $request['description'];

            if (file_exists($_FILES['image']['tmp_name']) || is_uploaded_file($_FILES['image']['tmp_name'])) {

                $image_is_uploaded = image_upload('image', 'images/promocode/', true, 'jpg|JPG|png|PNG|jpeg|JPEG');

                if (isset($image_is_uploaded['status']) && $image_is_uploaded['status']) {

                    $post['image'] = $image_is_uploaded['uploaded_path'];

                }

            }

            Promocode::where('id', $id)->update($post);

            $this->session->set_flashdata('success', __('Promocode Update successfully'));

            redirect('admin/promocode');

        }

        $this->renderAdmin('promocode/edit', $data);

    }

    public function delete($id) 

    {        

        $post['deleted_at'] = current_date();

        $deleted_record = Promocode::where('id', $id)->update($post);

        if (isset($deleted_record) && $deleted_record > 0) {

            $json_data = 1;

        } else {

            $json_data = 0;

        }

        echo json_encode($json_data);

    }

}



