<?php
defined('BASEPATH') or exit('No direct script access allowed');

class UserController extends MY_Controller

{

    function __construct()

    {

        parent::__construct();

        if (!$this
            ->ion_auth
            ->is_admin())

        {

            redirect('/');

        }

        $this->load->model('User');

        $this->load->model('ion_auth_model');
        $this->table_name = "users";

        $this->title = "Users";

        $this->user_grp = $this->config->item('roles', 'ion_auth') ['user'];
        $this->provider_grp = $this->config->item('roles', 'ion_auth') ['provider'];
    }

    public function index()

    {

        $data['title'] = $this->title;

        $this
        ->load
        ->library('Datatables');

        $product = new Datatables;

        $product->select('u.id, u.name, u.phone, u.email, u.active ', false)
        ->from($this->table_name . ' as u')
        ->join('users_groups as ug', 'u.id = ug.user_id')
        ->where('ug.group_id', $this->user_grp)
        ->where('u.deleted_at', NULL);

        $action['edit'] = base_url('admin/users/edit/');
        $action['delete'] = base_url('admin/users/delete/');
        $action['view'] = base_url('admin/users/view/');

        $product->style(['class' => 'table table-striped table-bordered nowrap'])

        ->column('#', 'id')

        ->column('Name', 'name')

        ->column('Phone', 'phone')

        ->column('Email', 'email')
        ->column('Approve', 'active', function ($active, $row)

        {

            if ($active == 1)

            {

                return '<h5 class="mb-0 mt-0"><span onclick="fun_change_state(this);" class="badge badge-success cursor-pointer font-15 status_' . $row['id'] . '" data-table="' . $this->table_name . '" data-id="' . $row['id'] . '">Active</span></h5>';

            }

            else

            {

                return '<h5 class="mb-0 mt-0"><span onclick="fun_change_state(this);" class="badge badge-danger cursor-pointer font-15 status_' . $row['id'] . '" data-table="' . $this->table_name . '" data-id="' . $row['id'] . '">Inactive</span></h5>';

            }

        })
        ->column('Actions', 'id', function ($id) use ($action)

            {

                $option = '<a href="' . $action['view'] . $id . '"  class="on-default text-green pr-1" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "View Record" data-rediret-url="' . current_url() . '"  data-table="' . $this->table_name . '" data-href="' . $this->table_name . '"><div class="btn-group btn-group-sm" style="float: none;"><button type="button" class="btn btn-info waves-effect waves-light" style="float: none;"><span class="dripicons-preview"></span></button></div></a>';

                $option .= '<a href="' . $action['edit'] . $id . '"><div class="btn-group btn-group-sm" style="float: none;"><button type="button" class="tabledit-edit-button btn btn-success" style="float: none;"><span class="mdi mdi-pencil"></span></button></div> </a>';

                $option .= '<a data-href="' . $action['delete'] . $id . '" href="javascript:void(0);" onclick="delete_confirm(this);"  class="on-default text-danger confirm_model" data-toggle="tooltip" data-placement="bottom" title="" data-original-title = "Delete Field" data-rediret-url="' . current_url() . '"  data-table="' . $this->table_name . '" ><div class="btn-group btn-group-sm" style="float: none;"><button type="button" class="btn btn-danger waves-effect waves-light" style="float: none;"><span class="mdi mdi-close"></span></button></div></a>';

                return $option;

            });

        $product->datatable($this->table_name);

        $product->init();

        $data['datatable'] = true;

        $data['export'] = false;

        $data['add_url'] = base_url('admin/users/add');

        $data['title'] = $this->title;

        $data['main_title'] = $this->title;

        $this->renderAdmin('users/index', $data);

    }

    public function add()

    {

        $data['title'] = "Users Add";

        $data['main_title'] = $this->title;

        $data['home'] = base_url('admin/users');

        if (isset($_POST) && !empty($_POST))

        {

            $request = $this
            ->input
            ->post();

            $id = $this
            ->ion_auth
            ->register($request['email'], $request['email'], $request['email'], $request, [2], 1);

            $this
            ->session
            ->set_flashdata('success', __('User Add successfully'));

            redirect('admin/users');

        }

        $this->renderAdmin('users/add', $data);

    }

    public function edit($id = '')

    {
        $users_ids = users_ids()->users_id;
        $users_id = explode(",", $users_ids);
        if (in_array($id, $users_id))
        {

            $data['users'] = User::find($id);

            $data['title'] = "Users Edit";

            $data['home'] = base_url('admin/users');

            $data['main_title'] = $this->title;

            if (isset($_POST) && !empty($_POST))

            {

                $request = $this
                ->input
                ->post();

                $post['name'] = $request['name'];
                $post['email'] = $request['email'];
                $post['phone'] = $request['phone'];
                $post['zipcode'] = $request['zipcode'];
                $post['gender'] = $request['gender'];

                User::whereId($id)->update($post);

                $this
                ->session
                ->set_flashdata('success', __('User Update successfully'));

                redirect('admin/users');

            }
            $this->renderAdmin('users/edit', $data);
        }
        else
        {
            redirect('admin/users');
        }

    }

    public function view($id = '')
    {
        $users_ids = users_ids()->users_id;
        $users_id = explode(",", $users_ids);
        if (in_array($id, $users_id))
        {
            $data['user'] = User::find($id);

            $data['title'] = "User View";
            $data['table'] = "users";

            $data['home'] = base_url('admin/users');

            $data['main_title'] = $this->title;            

            $this->renderAdmin('users/view', $data);
        }
        else
        {
            redirect('admin/users');
        }
    }

    public function delete($id) 

    {        

        $post['deleted_at'] = current_date();

        $deleted_record = User::where('id', $id)->update($post);

        if (isset($deleted_record) && $deleted_record > 0) {

            $json_data = 1;

        } else {

            $json_data = 0;

        }

        echo json_encode($json_data);

    }

}

