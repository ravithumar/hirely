<?php defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;

class Customer extends REST_Controller {

	function __construct() {

		header('Access-Control-Allow-Origin: *');

		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

		parent::__construct();
		$this->load->model('User');
		$this->load->model('Service');

		$this->load->model('ServiceImage');

		$this->load->model('SubCategory');

		$this->load->model('PaymentCard');

		$this->load->model('Category');

		$this->load->model('Claim');

		$this->load->model('ClaimSubject');
		$this->load->model('Availability');

		$this->load->library(['ion_auth', 'form_validation']);

		$this->user_grp = $this->config->item('roles', 'ion_auth') ['user'];
		$this->provider_grp = $this->config->item('roles', 'ion_auth') ['provider'];

	}

	public function home_get() {

		$category = Category::whereStatus('1')->whereDeletedAt(NULL)->skip(0)->take(5)->get();
		$service = Service::whereStatus('1')->whereDeletedAt(NULL)->skip(0)->take(5)->get();
		$service_arr = [];
		foreach ($service as $key => $row) {
			$service_arr[$key] = $row;
			$service_arr[$key]['rating'] = rating($row->provider_id);
		}

		$data['status'] = true;

		$data['data']['category'] = $category;
		$data['data']['service'] = $service_arr;

		$this->response($data);

	}

	public function category_view_all_get() {
    	$category = Category::whereStatus('1')->whereDeletedAt(NULL)->get();
		$data['status'] = true;
		$data['data'] = $category;
		$this->response($data);
	}

	public function claim_subject_get() {
		$claim_subject = ClaimSubject::whereStatus('1')->whereDeletedAt(NULL)->get();
		$data['status'] = true;
		$data['data'] = $claim_subject;
		$this->response($data);
	}

	public function claim_post() {
		$this->form_validation->set_rules('user_id', 'User Id', 'required');
		$this->form_validation->set_rules('order_id', 'Order Id', 'required');
		$this->form_validation->set_rules('reason', 'Reason', 'required');
		$this->form_validation->set_rules('description', 'Description', 'required');
		if ($this->form_validation->run() == FALSE) {
			$data['status'] = false;
			$data['error'] = validation_errors_response();
		} else {
			$request = $this->input->post();
			$post['user_id'] = $request['user_id'];
			$post['reason'] = $request['reason'];
			$post['description'] = $request['description'];
			$post['order_id'] = $request['order_id'];
			if (isset($_FILES['images']['name']) && $_FILES['images']['name'] != '') {
				$image_data = array($_FILES['images']);
				if ($image_data[0]['name'] != "") {
					$all_images = array();
					$files = $_FILES;
					$count = count($_FILES['images']['name']);
					for ($i = 0; $i < $count; $i++) {
						$_FILES['images']['name'] = $files['images']['name'][$i];
						$_FILES['images']['type'] = $files['images']['type'][$i];
						$_FILES['images']['tmp_name'] = $files['images']['tmp_name'][$i];
						$_FILES['images']['error'] = $files['images']['error'][$i];
						$_FILES['images']['size'] = $files['images']['size'][$i];
						$image_is_uploaded = image_upload('images', 'images/claim/', true, 'jpg|JPG|png|PNG|jpeg|JPEG');
						$img_array = [];
						if ($image_is_uploaded['status']) {
							$all_images[] = $image_is_uploaded['uploaded_path'];
						}
					}
				}
			}
			$post['image'] = implode(", ", $all_images);
			$last_insert_id = Claim::insertGetId($post);

			if (isset($last_insert_id) && !empty($last_insert_id)) {
				$data['status'] = true;
				$data['message'] = 'Claim Add successfully';
			} else {
				$data['status'] = false;
				$data['error'] = "Something Went Wrong";
			}
		}
		$this->response($data);
	}

	public function add_payment_card_post() {

		$this->form_validation->set_rules('user_id', 'user id', 'trim|required');
		$this->form_validation->set_rules('card_holder_name', 'card holder name', 'trim|required');
		$this->form_validation->set_rules('card_number', 'card number', 'trim|required');
		$this->form_validation->set_rules('expiry_date', 'expiry date', 'trim|required');
		$this->form_validation->set_rules('cvv', 'cvv', 'trim|required');
		$this->form_validation->set_rules('card_type', 'card type', 'trim|required');
		if ($this->form_validation->run() == false) {
			$data['status'] = false;
			$data['message'] = validation_errors_response();
		} else {
			$request = $this->post();
			$card_number = str_replace(' ', '', $request['card_number']);
			$validate_date = validate_expirydate($request['expiry_date']);
			if ($validate_date) {
				$card_types = array(
					'1' => 'visa',
					'2' => 'mastercard',
					'3' => 'amex',
					'4' => 'jcb',
					'5' => 'dinnerclub',
					'6' => 'discover',
				);
				$my_card_type = $request['card_type'];
				$card_type = validate_customer_card($card_number);

				if ($card_type != '' && $card_type == $card_types[$my_card_type]) {
					$check_card_is_exist = PaymentCard::whereUserId($request['user_id'])->whereCardNumber(encrypt($card_number))->whereDeletedAt(NULL)->count();

					if (!empty($check_card_is_exist) && count($check_card_is_exist) > 0) {
						$data['status'] = false;
						$data['message'] = 'Card already exists';
					} else {
						$payment_card_data = array(
							'user_id' => $request['user_id'],
							'card_holder_name' => ucfirst(addslashes($request['card_holder_name'])),
							'card_number' => encrypt($card_number),
							'display_number' => 'XXXX XXXX XXXX ' . substr($card_number, -4),
							'expiry_date' => encrypt($request['expiry_date']),
							'cvv' => encrypt(intval($request['cvv'])),
							'card_type' => intval($request['card_type']),
							'created_at' => current_date(),
						);
						$last_insert_id = PaymentCard::insertGetId($payment_card_data);
						$data['status'] = true;
						$data['message'] = 'Card added successfully';
					}
				} else {
					$data['status'] = false;
					$data['message'] = 'Invalid card number';
				}
			} else {
				$data['status'] = false;
				$data['message'] = 'Card is already expired';
			}
		}
		$this->response($data);
	}
	public function my_payment_cards_post() {
		$this->form_validation->set_rules('user_id', 'user id', 'required');
		if ($this->form_validation->run() == false) {
			$data['status'] = false;
			$data['message'] = validation_errors_response();
		} else {
			$request = $this->post();
			$cards = PaymentCard::whereUserId($request['user_id'])->whereDeletedAt(NULL)->get();
			if (count($cards) > 0) {
				foreach ($cards as $key => $value) {
					$row[$key] = $value;
					$row[$key]['display_expiry_date'] = decrypt($value['expiry_date']);
					$row[$key]['card_number'] = isset($value->card_number) ? decrypt($value->card_number) : '';
					$row[$key]['expiry_date'] = isset($value->expiry_date) ? decrypt($value->expiry_date) : '';
					$row[$key]['cvv'] = isset($value->cvv) ? decrypt($value->cvv) : '';
				}
				$data['cards'] = $row;
				$data['status'] = true;
				$data['message'] = '';
			} else {
				$data['cards'] = [];
				$data['status'] = false;
				$data['message'] = 'Card not found';
			}
		}
		$this->response($data);
	}
	public function card_delete_post() {
		$this->form_validation->set_rules('card_id', 'card id', 'required');
		if ($this->form_validation->run() == false) {
			$data['status'] = false;
			$data['message'] = validation_errors_response();
		} else {
			$request = $this->post();
			$post['deleted_at'] = current_date();
			$response = PaymentCard::whereId($request['card_id'])->update($post);
			$data['status'] = true;
			$data['message'] = "Card deleted successfully";
		}
		$this->response($data);
	}

	public function find_zipcode_post() {
		$this->form_validation->set_rules('zipcode', 'Zip Code', 'required');
		if ($this->form_validation->run() == false) {
			$data['status'] = false;
			$data['message'] = validation_errors_response();
		} else {
			$request = $this->post();						
			$users = User::groupAllProvider($this->provider_grp)->whereRaw('find_in_set(?, zipcode)', [$request['zipcode']])->get();
			$service = Service::whereStatus('1')->get();
			$find_services = [];
			foreach ($users as $key => $row) {
				foreach ($service as $key1 => $value) {
					if ($row->id == $value->user_id) {
						$find_services[] = $value;
					}
				}
			}
			$data['status'] = true;
			$data['message'] = "Successfully";
			$data['data'] = $find_services;			
		}
		$this->response($data);
	}

	public function service_deatils_get($service_id = '') {

		$service = Service::whereId($service_id)->first();
		$service_images = ServiceImage::whereServiceId($service->id)->get();
		$availability = Availability::whereProviderId($service->user_id)->first();		
		$service_images_arr = array_column((array)$service_images, 'image');
		
		$duration = 60; 
		$start    = '15:00'; 
		$end      = '20:00'; 
		 
		$date = date('Y-m-d');		
		$day = strtolower(date('D',strtotime(date('Y-m-d'))));
		$ts = strtotime($date);		
		$year = date('o', $ts);
		$week = date('W', $ts);		
		for($i = 1; $i <= 7; $i++) {		    
		    $ts = strtotime($year.'W'.$week.$i);
		    $date_arr[] = date("Y-m-d", $ts);
		    $day_arr[] = date("l", $ts);
		    $day_short_name[] = date("D", $ts);
			$time_slot[] = $this->getTimeSlots($duration, $start, $end, strtolower(date("D", $ts)), $service->user_id);

		}
		_pre($time_slot);
		$slot = $availability;

		$data['status'] = true;
		$data['data']['service'] = $service;
		$data['data']['service']['images'] = $service_images_arr;
		$data['data']['availability']['date'] = $date_arr;
		$data['data']['availability']['day'] = $day_arr;
		$data['data']['availability']['slot'] = $slot;
		$this->response($data);

	}

	function getTimeSlots($duration = '', $start = '', $end = '', $day = '', $provider_id = '')
	{

		 $availability_time = Availability::whereProviderId($provider_id)->first();
		 // echo $day;
		 if($availability_time->$day != "")
		 {

		 }

        // $time = array();
        // $start = new \DateTime($start);
        // $end = new \DateTime($end);
        // $start_time = $start->format('H:i');
        // $end_time = $end->format('H:i');
        // $currentTime = strtotime(Date('Y-m-d H:i'));
        // $i=0;	
 
        // while(strtotime($start_time) <= strtotime($end_time)){
        //     $start = $start_time;
        //     $end = date('H:i',strtotime('+'.$duration.' minutes',strtotime($start_time)));
        //     $start_time = date('H:i',strtotime('+'.$duration.' minutes',strtotime($start_time)));
            
        //     $today = Date('Y-m-d');
        //     $slotTime = strtotime($today.' '.$start);
 
        //     if($slotTime > $currentTime){
        //         if(strtotime($start_time) <= strtotime($end_time)){        //        
        //             $time[$i]['start'] = $start;
        //             $time[$i]['end'] = $end;
        //         }
        //         $i++;
        //     }
 
        // }
        // return $time;
	}

}
		