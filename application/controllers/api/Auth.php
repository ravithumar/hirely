<?php defined('BASEPATH') OR exit('No direct script access allowed');

use Restserver\Libraries\REST_Controller;

class Auth extends REST_Controller {

	function __construct() {

		header('Access-Control-Allow-Origin: *');

		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");

		parent::__construct();

		$this->load->model('User');

		$this->load->model('Devices');

		$this->load->model('Availability');

		$this->load->model('ion_auth_model');

		$this->load->library(['ion_auth', 'form_validation']);

	}

	public function login_post() {

		$this->form_validation->set_rules('phone', 'phone', 'required');

		$this->form_validation->set_rules('type', 'type', 'required');

		$this->form_validation->set_rules('device_token', 'device token', 'required');

		$this->form_validation->set_rules('device_name', 'device name', 'required');

		$this->form_validation->set_rules('device_type', 'device type', 'required');

		if ($this->form_validation->run() == FALSE) {

			$data['status'] = false;

			$data['error'] = $this->validation_errors_response();

		} else {

			$request = $this->post();

			$type = $request['type'];

			$login = $this->ion_auth->api_user_login($request['phone'], $request['device_token'], $request['device_name'],FALSE,$type);
			
			if ($login) {

				if ($login->active == 1) {

					$login->api_key = $this->_generate_key($login->id);

					$data['status'] = true;

					$data['user'] = $login;

				} else {

					$data['status'] = false;

					$data['error'] = "This account is not activated!";

				}

			} else {

				$data['status'] = false;

				$data['error'] = "This mobile number is not registered";

			}

		}

		$this->response($data);

	}

	public function user_register_post() {

		$this->form_validation->set_rules('name', 'name', 'required');

		$this->form_validation->set_rules('email', 'email', 'required|is_unique[users.email]', array('is_unique' => __('This email address is already exist!.')));

		$this->form_validation->set_rules('phone', 'phone', 'required|is_unique[users.phone]', array('is_unique' => __('This phone is already exist!.')));

		$this->form_validation->set_rules('gender', 'gender', 'required');

		// $this->form_validation->set_rules('password', 'password', 'required');

		$this->form_validation->set_rules('device_token', 'device token', 'required');

		$this->form_validation->set_rules('device_name', 'device name', 'required');

		$this->form_validation->set_rules('device_type', 'device type', 'required');

		if ($this->form_validation->run() == FALSE) {

			$data['status'] = false;

			$data['error'] = $this->validation_errors_response();

		} else {

			$request = $this->post();

			$email = $request['email'];

			// $password = $request['password'];
			// unset($request['password']);

			unset($request['email']);

			$id = $this->ion_auth->register($email, $email, $email, $request, [2], 1);

			Devices::insert(['user_id' => $id, 'name' => $request['device_name'], 'token' => $request['device_token'], 'type' => $request['device_type']]);
			$wallet = add_wallet($id, 'user');

			$user = User::find($id);

			$user->api_key = $this->_generate_key($id);

			// send_mail("Verify account",$user->email,'register_temp',$user);

			$data['status'] = true;

			$data['data'] = $user;

		}

		$this->response($data);

	}

	public function provider_register_post() {

		// echo "here";exit();

		$this->form_validation->set_rules('name', 'name', 'required');

		$this->form_validation->set_rules('email', 'email', 'required|is_unique[users.email]', array('is_unique' => __('This email address is already exist!.')));

		$this->form_validation->set_rules('phone', 'phone', 'required|is_unique[users.phone]', array('is_unique' => __('This phone is already exist!.')));

		$this->form_validation->set_rules('gender', 'gender', 'required');

		$this->form_validation->set_rules('zipcode', 'Zip Code', 'required');

		$this->form_validation->set_rules('type', 'Type', 'required');

		$this->form_validation->set_rules('device_token', 'device token', 'required');

		$this->form_validation->set_rules('device_name', 'device name', 'required');

		$this->form_validation->set_rules('device_type', 'device type', 'required');

		if ($this->form_validation->run() == FALSE) {

			$data['status'] = false;

			$data['error'] = $this->validation_errors_response();

		} else {

			$request = $this->post();

			$email = $request['email'];

			unset($request['email']);

			$id = $this->ion_auth->register($email, $email, $email, $request, [3], 1);

			Devices::insert(['user_id' => $id, 'name' => $request['device_name'], 'token' => $request['device_token'], 'type' => $request['device_type']]);

			Availability::insert(['provider_id' => $id]);
			$user = User::find($id);
			$wallet = add_wallet($id, 'provider');

			$user->api_key = $this->_generate_key($id);			

			$data['status'] = true;

			$data['data'] = $user;

		}

		$this->response($data);

	}

	public function send_otp_post() {

		$this->form_validation->set_rules('phone', 'mobile', 'required');

		$this->form_validation->set_rules('email', 'email', 'required');

		if ($this->form_validation->run() == FALSE) {

			$data['status'] = false;

			$data['error'] = $this->validation_errors_response();

		} else {

			$request = $this->post();

			$check_mobile = User::wherePhone($request['phone'])->first();

			$check_email = User::whereEmail($request['email'])->first();

			if (!empty($check) && $check_email) {

				$data['status'] = false;

				$data['error'] = "This mobile & email is already registered!.";

			} 

			else if (!empty($check_mobile)) {

				$data['status'] = false;

				$data['error'] = "This mobile number is already registered!.";

			}

			else if (!empty($check_email)) {

				$data['status'] = false;

				$data['error'] = "This email is already registered!.";

			}

			else {

				$OTP = rand(1000, 9999);

				sendEmail('HJM OTP',$request['email'],'otp',['otp'=>$OTP]);

				$data['status'] = true;

				$data['data'] = $OTP;

			}

		}

		$this->response($data);

	}

	public function forgot_password_post() {

		$this->form_validation->set_rules('email', 'email', 'required');

		if ($this->form_validation->run() == FALSE) {

			$data['status'] = false;

			$data['error'] = $this->validation_errors_response();

		} else {

			$email = $this->post('email');

			$user = User::whereEmail($email)->first();

			if (empty($user)) {

				$data['status'] = false;

				$data['error'] = "This email address is not registered";

			} else {

				$user_meta['link'] = base_url('reset/password/' . urlencode($email) . '/' . time());

				sendEmail('Reset password', 'developer.eww@gmail.com', 'forgot_password', $user_meta);

				$data['status'] = true;

				$data['data'] = "success";

			}

		}

		$this->response($data);

	}

	public function _generate_key($uid) {

		do {

			// Generate a random salt

			$salt = base_convert(bin2hex($this->security->get_random_bytes(64)), 16, 36);

			// If an error occurred, then fall back to the previous method

			if ($salt === FALSE) {

				$salt = hash('sha256', time() . mt_rand());

			}

			$new_key = substr($salt, 0, config_item('rest_key_length'));

		} while ($this->_key_exists($new_key));

		$this->_insert_key($new_key, ['level' => 1, 'ignore_limits' => 1, 'user_id' => $uid]);

		return $new_key;

	}

	private function _key_exists($key) {

		return $this->rest->db

			->where(config_item('rest_key_column'), $key)

			->count_all_results(config_item('rest_keys_table')) > 0;

	}

	private function _insert_key($key, $data) {

		$data[config_item('rest_key_column')] = $key;

		$data['date_created'] = function_exists('now') ? now() : time();

		$check_key = $this->db->get_where('tokens', array('user_id' => $data['user_id']))->row();

		if (empty($check_key)) {

			return $this->rest->db

				->set($data)

				->insert(config_item('rest_keys_table'));

		} else {

			$this->_update_key($key, $data['user_id']);

		}

	}

	private function _update_key($key, $uid) {

		return $this->rest->db

			->where('user_id', $uid)

			->update(config_item('rest_keys_table'), array('date_created' => time(), 'token' => $key));

	}

	private function _delete_key($key) {

		return $this->rest->db

			->where(config_item('rest_key_column'), $key)

			->delete(config_item('rest_keys_table'));

	}

	public function validation_errors_response() {

		$err_array = array();

		$err_str = "";

		$err_str = str_replace(array('<p>', '</p>'), array('|', ''), trim(validation_errors()));

		$err_str = ltrim($err_str, '|');

		$err_str = rtrim($err_str, '|');

		$err_array = explode('|', $err_str);

		$err_array = array_filter($err_array);

		return $err_array;

	}

	public function testdistance_get() {

		echo $this->getDistanceBetweenPointsNew('23.100878', '72.5347998', '23.0504568', '72.4594858');exit;

	}

	function getDistanceBetweenPointsNew($latitude1, $longitude1, $latitude2, $longitude2, $unit = 'Km') {

		$theta = $longitude1 - $longitude2;

		$distance = (sin(deg2rad($latitude1)) * sin(deg2rad($latitude2))) + (cos(deg2rad($latitude1)) * cos(deg2rad($latitude2)) * cos(deg2rad($theta)));

		$distance = acos($distance);

		$distance = rad2deg($distance);

		$distance = $distance * 60 * 1.1515;switch ($unit) {

		case 'Mi':break;case 'Km':$distance = $distance * 2.509322;

		}

		return (round($distance, 2));

	}

	public function distanceCalculation($point1_lat, $point1_long, $point2_lat, $point2_long, $unit = 'km', $decimals = 2) {

		// Calculate the distance in degrees

		$degrees = rad2deg(acos((sin(deg2rad($point1_lat)) * sin(deg2rad($point2_lat))) + (cos(deg2rad($point1_lat)) * cos(deg2rad($point2_lat)) * cos(deg2rad($point1_long - $point2_long)))));

		// Convert the distance in degrees to the chosen unit (kilometres, miles or nautical miles)

		switch ($unit) {

		case 'km':

			$distance = $degrees * 111.13384; // 1 degree = 111.13384 km, based on the average diameter of the Earth (12,735 km)

			break;

		case 'mi':

			$distance = $degrees * 69.05482; // 1 degree = 69.05482 miles, based on the average diameter of the Earth (7,913.1 miles)

			break;

		case 'nmi':

			$distance = $degrees * 59.97662; // 1 degree = 59.97662 nautic miles, based on the average diameter of the Earth (6,876.3 nautical miles)

		}

		return round($distance, $decimals);

	}

}