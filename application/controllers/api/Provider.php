<?php defined('BASEPATH') OR exit('No direct script access allowed');
use Restserver\Libraries\REST_Controller;
class Provider extends REST_Controller {
	function __construct() {
		header('Access-Control-Allow-Origin: *');
		header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		parent::__construct();
		$this->load->model('User');		
		$this->load->model('Service');		
		$this->load->model('ServiceImage');
		$this->load->model('SubCategory');
        $this->load->model('Category');        
        $this->load->model('Availability');
        $this->load->model('BankAccount');
		$this->load->library(['ion_auth', 'form_validation']);
	}
	public function add_service_post() 
	{
		$this->form_validation->set_rules('user_id', 'User Id', 'required');
		$this->form_validation->set_rules('category', 'Category', 'required');
		$this->form_validation->set_rules('sub_category', 'Sub Category', 'required');
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('description', 'Description', 'required');
		$this->form_validation->set_rules('fixed_price', 'Fixed price', 'required');
		$this->form_validation->set_rules('booking_price', 'Booking price', 'required');
		if ($this->form_validation->run() == FALSE) 
		{
			$data['status'] = false;
			$data['error'] = validation_errors_response();
		} 
		else 
		{
			$request = $this->input->post();
            $post['user_id'] = $request['user_id'];
            $post['category_id'] = $request['category'];
            $post['sub_category_id'] = $request['sub_category'];
            $post['title'] = $request['title'];
            $post['description'] = $request['description'];
            $post['booking_price'] = $request['booking_price'];
            $post['fixed_price'] = $request['fixed_price'];            
            $last_insert_id = Service::insertGetId($post);
        						
            if (isset($_FILES['images']['name']) && $_FILES['images']['name'] != '') 
            {
				$image_data = array($_FILES['images']);
				if ($image_data[0]['name'] != "") 
				{
					$all_images = array();						
					$files = $_FILES;
					$count = count($_FILES['images']['name']);					
					for ($i = 0; $i < $count; $i++) 
					{
						$_FILES['images']['name'] = $files['images']['name'][$i];
						$_FILES['images']['type'] = $files['images']['type'][$i];
						$_FILES['images']['tmp_name'] = $files['images']['tmp_name'][$i];
						$_FILES['images']['error'] = $files['images']['error'][$i];
						$_FILES['images']['size'] = $files['images']['size'][$i];
						$image_is_uploaded = image_upload('images', 'images/service/', true, 'jpg|JPG|png|PNG|jpeg|JPEG');
						$img_array = [];
						if ($image_is_uploaded['status'])
		                {
		                    $all_images[] = $image_is_uploaded['uploaded_path']; 
		                }
					}
					foreach ($all_images as $value) 
					{
						$add_more['image'] = $value;
						$add_more['service_id'] = $last_insert_id;
						$service_image_id = ServiceImage::insert($add_more);
					}
				}
			}            
			if (isset($last_insert_id) && !empty($last_insert_id)) 
			{
				$data['status'] = true;
				$data['message'] = 'Service Add successfully';
			} 
			else 
			{
				$data['status'] = false;
				$data['error'] = "Something Went Wrong";
			}
		}
		$this->response($data);
	}	

	public function category_get() {
    	$category = Category::whereStatus('1')->whereDeletedAt(NULL)->get();
		$data['status'] = true;
		$data['data'] = $category;
		$this->response($data);
	}

	public function sub_category_get($category_id="") 
	{			
		if (isset($category_id) && !empty($category_id)) 
		{			
            $sub_category = SubCategory::whereCategoryId($category_id)->whereStatus('1')->whereDeletedAt(NULL)->get();
			$data['status'] = true;
			$data['data'] = $sub_category;
		} 
		else 
		{
			$data['status'] = false;
			$data['error'] = 'Please Enter Category Id';
		}
		$this->response($data);
	}

	public function edit_service_post() 
	{
		$this->form_validation->set_rules('user_id', 'User Id', 'required');
		$this->form_validation->set_rules('service_id', 'Service Id', 'required');
		$this->form_validation->set_rules('category', 'Category', 'required');
		$this->form_validation->set_rules('sub_category', 'Sub Category', 'required');
		$this->form_validation->set_rules('title', 'Title', 'required');
		$this->form_validation->set_rules('description', 'Description', 'required');
		$this->form_validation->set_rules('fixed_price', 'Fixed price', 'required');
		$this->form_validation->set_rules('booking_price', 'Booking price', 'required');
		if ($this->form_validation->run() == FALSE) 
		{
			$data['status'] = false;
			$data['error'] = validation_errors_response();
		}
		else
	 	{
			$request = $this->input->post();
			$service_id = $request['service_id'];
            $post['user_id'] = $request['user_id'];
            $post['category_id'] = $request['category'];
            $post['sub_category_id'] = $request['sub_category'];
            $post['title'] = $request['title'];
            $post['description'] = $request['description'];
            $post['booking_price'] = $request['booking_price'];
            $post['fixed_price'] = $request['fixed_price'];            
            $response = Service::where('id', $service_id)->update($post);

            if (isset($_FILES['images']['name']) && $_FILES['images']['name'] != '') 
            {
				$image_data = array($_FILES['images']);
				if ($image_data[0]['name'] != "") 
				{
					$all_images = array();						
					$files = $_FILES;
					$count = count($_FILES['images']['name']);					
					for ($i = 0; $i < $count; $i++) 
					{
						$_FILES['images']['name'] = $files['images']['name'][$i];
						$_FILES['images']['type'] = $files['images']['type'][$i];
						$_FILES['images']['tmp_name'] = $files['images']['tmp_name'][$i];
						$_FILES['images']['error'] = $files['images']['error'][$i];
						$_FILES['images']['size'] = $files['images']['size'][$i];
						$image_is_uploaded = image_upload('images', 'images/service/', true, 'jpg|JPG|png|PNG|jpeg|JPEG');
						$img_array = [];
						if (isset($image_is_uploaded['status']) && $image_is_uploaded['status'])
		                {
		                    $all_images[] = $image_is_uploaded['uploaded_path']; 
		                }
					}
					foreach ($all_images as $key => $value) 
					{
						$add_more['image'] = $value;
						$add_more['service_id'] = $service_id;
						$add_more['created_at'] = date('Y-m-d');
						$service_image_id = ServiceImage::insert($add_more);
					}
				}
			}
			if (isset($response) && !empty($response)) 
			{
				$data['status'] = true;
				$data['message'] = 'Service Update successfully';
			} 
			else 
			{
				$data['status'] = false;
				$data['error'] = "Something Went Wrong";
			}
		}
		$this->response($data);
	}	

	public function my_service_get($user_id = '') 
	{
		
		if (isset($user_id) && !empty($user_id)) 
		{
			$request = $this->input->post();
            $my_service = Service::where('user_id', $user_id)->get();			            
			
			$data['status'] = true;
			$data['message'] = 'successfully';
			$data['data'] = $my_service;
		}
		else
	 	{
			$data['status'] = false;
			$data['error'] = "Please Enter User Id";
		}
		$this->response($data);
	}

	public function edit_profile_post() {		
		$this->form_validation->set_rules('user_id', 'User Id', 'required');
		$this->form_validation->set_rules('name', 'Name', 'required');		
		$this->form_validation->set_rules('email', 'email', 'required|trim|callback_isexists[' . $this->post('user_id') . ']');
		$this->form_validation->set_rules('phone', 'phone', 'required|trim|callback_isexists[' . $this->post('user_id') . ']');
		$this->form_validation->set_rules('gender', 'Gender', 'required');
		$this->form_validation->set_rules('zipcode', 'Zip Code', 'required');
		if ($this->form_validation->run() == FALSE) {
			$data['status'] = false;
			$data['error'] = validation_errors_response();
		} else {
			$request = $this->post();
			$post['name'] = $request['name'];
            $post['email'] = $request['email'];
            $post['phone'] = $request['phone'];
            $post['gender'] = $request['gender'];
            $post['zipcode'] = $request['zipcode'];

            if (isset($_FILES['image']['name']) && $_FILES['image']['name'] != '') 
            {
            	$image_is_uploaded = image_upload('image', 'images/provider_profile/', true, 'jpg|JPG|png|PNG|jpeg|JPEG');
				if (isset($image_is_uploaded['status']) && $image_is_uploaded['status'])
                {
                    $post['image'] = $image_is_uploaded['uploaded_path'];
                }
			}
            User::whereId($request['user_id'])->update($post);

            $user = User::whereId($request['user_id'])->get();
            if (isset($user) && !empty($user)) 
            {
				$data['status'] = true;				
				$data['message'] = "Profile Update Successfully";				
				$data['data'] = $user;            	
            }
            else
            {
            	$data['status'] = false;
				$data['error'] = "Something Went Wrong";
            }
		}
		$this->response($data);
	}

	public function isexists($str = NULL, $id = NULL) {		
		$this->db->select('*');
		$this->db->group_start();
		$this->db->where('email', trim($str));
		$this->db->or_where('phone', trim($str));
		$this->db->group_end();				
		if (!is_null($id)) {
			$this->db->where_not_in('id', $id);
		}
		$this->db->from('users');
		$this->db->limit(1);
		$sql_query = $this->db->get();
		if ($sql_query->num_rows() > 0) {
			$this->form_validation->set_message('isexists', (is_numeric($str)) ? "Contact number is already exist" : "The email is already exists");
			return false;
		} else {
			return true;
		}
	}

	public function near_by_service_post() 
	{		
		$this->form_validation->set_rules('latitude', 'Latitude', 'required');
		$this->form_validation->set_rules('longitude', 'Longitude', 'required');
		if ($this->form_validation->run() == FALSE) 
		{
			$data['status'] = false;
			$data['error'] = validation_errors_response();
		}
		else
	 	{
			$request = $this->input->post();                     
            $coordinates = array(
            	"latitude" => $request['latitude'],
            	"longitude" => $request['longitude']
            );
            $near_by_service = User::where('active', 1)
	         ->isWithinMaxDistance($coordinates)
	           ->orderBy('distance')->get();
			if (isset($near_by_service) && !empty($near_by_service)) 
			{
				$data['status'] = true;
				$data['message'] = 'Successfully';
				$data['data'] = $near_by_service;
			} 
			else 
			{
				$data['status'] = false;
				$data['error'] = "Something Went Wrong";
			}
		}
		$this->response($data);
	}	

	public function availability_post() 
	{
		$this->form_validation->set_rules('user_id', 'User Id', 'required');
		$this->form_validation->set_rules('day', 'Day', 'required');
		$this->form_validation->set_rules('all_day', 'All Day', 'required');
		$this->form_validation->set_rules('slot', 'Slot', 'required');

		if ($this->form_validation->run() == FALSE) 
		{
			$data['status'] = false;
			$data['error'] = validation_errors_response();
		} 
		else 
		{
			$request = $this->input->post();
			$slot_data = json_decode($request['slot'], true);			
            $post['provider_id'] = $request['user_id'];
            $post['all_day'] = $request['all_day'];
            $post['all_day'] = $request['all_day'];
			foreach ($slot_data as $row) {
				$to[] = $row['to'];
				$from[] = $row['from'];
			}
			$post[$request['day']] = $this->get_time($to, $from);			
            $last_insert_id = Availability::whereProviderId($request['user_id'])->update($post);
                    
			if (isset($last_insert_id) && !empty($last_insert_id)) 
			{
				$data['status'] = true;
				$data['message'] = 'Availability Update successfully';
			} 
			else 
			{
				$data['status'] = false;
				$data['error'] = "Something Went Wrong";
			}
		}
		$this->response($data);
	}

	public function get_time($start = '' , $end = '')
	{
		for ($i = 0; $i < count($start); $i++) 
		{
			if ($start[$i] !='' && $end[$i] !='') 
			{
				$arr[$i] = $start[$i] . '-' . $end[$i];	
			}
		}

		if (!empty($arr)) 
		{
			return  implode("|", $arr);
		}
		else
		{
			return;
		}

	}

	public function add_bank_account_details_post() {
        $this
            ->form_validation
            ->set_rules('user_id', 'user id', 'trim|required');
        $this
            ->form_validation
            ->set_rules('account_number', 'Account Number', 'trim|required');        
        $this
            ->form_validation
            ->set_rules('bank_name', 'Bank name', 'trim|required');
        $this
            ->form_validation
            ->set_rules('ifsc_code', 'IFSC Code', 'trim|required');
        $this
            ->form_validation
            ->set_rules('account_holder_name', 'Account Holder Name', 'trim|required');
        if ($this
            ->form_validation
            ->run() == false) {
            $data['status'] = false;
            $data['message'] = validation_errors_response();
        } else {
            $request = $this->post();
            $account_number = str_replace(' ', '', $request['account_number']);            
            $check_card_is_exist = BankAccount::whereUserId($request['user_id'])->whereDeletedAt(NULL)->count();            
            if (!empty($check_card_is_exist) && count($check_card_is_exist) > 0) {
                $data['status'] = false;
                $data['message'] = 'Bank Account already exists';                        
            } else {
                $bank_account_details = array(
                    'user_id' => $request['user_id'],
                    'account_holder_name' => ucfirst(addslashes($request['account_holder_name'])),
                    'bank_name' => ucfirst(addslashes($request['bank_name'])),                                        
                    'account_number' => encrypt($account_number),
                    'display_number' => 'XXXX XXXX XXXX ' . substr($account_number, -4),		
                    'ifsc_code' => encrypt($request['ifsc_code']),                            
                    'created_at' => current_date(),
                );
                $last_insert_id = BankAccount::insertGetId($bank_account_details);                
                $data['status'] = true;
                $data['message'] = 'Bank account details has been saved successfully';                        
            }
        }
        $this->response($data);
    }

    public function edit_bank_account_details_post() {
        $this
            ->form_validation
            ->set_rules('bank_account_id', 'Bank Account id', 'trim|required');
        $this
            ->form_validation
            ->set_rules('bank_name', 'Bank name', 'trim|required');        
        $this
            ->form_validation
            ->set_rules('ifsc_code', 'IfSC Code', 'trim|required');
        $this
            ->form_validation
            ->set_rules('account_holder_name', 'Account Holder Name', 'trim|required');
        $this
            ->form_validation
            ->set_rules('account_holder_name', 'Account Holder Name', 'trim|required');
        if ($this
            ->form_validation
            ->run() == false) {
            $data['status'] = false;
            $data['message'] = validation_errors_response();
        } else {
            $request = $this->post();
            $account_number = str_replace(' ', '', $request['account_number']);
            $bank_account_details = array(
                // 'user_id' => $request['user_id'],
                'account_holder_name' => ucfirst(addslashes($request['account_holder_name'])),
                'bank_name' => ucfirst(addslashes($request['bank_name'])),                
                'account_number' => encrypt($account_number),
                'display_number' => 'XXXX XXXX XXXX ' . substr($account_number, -4),	
                'ifsc_code' => encrypt($request['ifsc_code']),
                'created_at' => current_date(),
            );
            $response = BankAccount::whereId($request['bank_account_id'])->update($bank_account_details);
            $data['status'] = true;
            $data['message'] = 'Bank account details has been updated successfully';                        
        }
        $this->response($data);
    }
    public function my_bank_account_post() {
        $this
            ->form_validation
            ->set_rules('user_id', 'user id', 'required');       
        if ($this
            ->form_validation
            ->run() == false) {
            $data['status'] = false;
            $data['message'] = validation_errors_response();
        } else {
            $request = $this->post();
            $bank_account = BankAccount::whereUserId($request['user_id'])->first();
            if (!empty($bank_account)) {
                $data['bank_account'] = $bank_account;                                
                $data['bank_account']['account_number'] = isset($bank_account->account_number) ? decrypt($bank_account->account_number) : '';
                $data['bank_account']['ifsc_code'] = isset($bank_account->ifsc_code) ? decrypt($bank_account->ifsc_code) : '';
                $data['status'] = true;
                $data['message'] = '';
            } else {
                $object = array();
                $data['bank_account'] = $object = (object)array();
                $data['status'] = false;
                $data['message'] = 'Bank Account not found';                
            }
        }
        $this->response($data);
    }
    public function bank_account_delete_post() {
        $this
            ->form_validation
            ->set_rules('bank_account_id', 'Bank Account id', 'required');
        if ($this
            ->form_validation
            ->run() == false) {
            $data['status'] = false;
            $data['message'] = validation_errors_response();
        } else {
            $request = $this->post();            
            $post['deleted_at'] = current_date();			
			$response = BankAccount::whereId($request['bank_account_id'])->update($post);
            $data['status'] = true;
            $data['bank_account'] = $request['bank_account_id'];
            $data['message'] = "Bank Account deleted successfully.";            
        }
        $this->response($data);
    }

}