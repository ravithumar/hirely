<?php defined('BASEPATH') OR exit('No direct script access allowed');
    
use Restserver\Libraries\REST_Controller;
class Trucks extends REST_Controller {
	function __construct()
	{
        header('Access-Control-Allow-Origin: *');
        header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
		parent::__construct();
		$this->load->helper('Api_helper');
		$this->load->model('TruckType');
	}
	public function list_get($type="",$lat,$lang)
	{
		if($type=="")
		{
			$types = TruckType::whereStatus(1)->whereType('FTL')->orderBy('id','desc')->get();
		}
		else
		{
			$types = TruckType::whereStatus(1)->whereType($type)->orderBy('id','desc')->get();	
		}
		$data['status'] = true;
		$data['data'] = $types;
		$this->response($data);
	}
	public function get_fare_post()
	{
		$this->form_validation->set_rules('truck_type', 'truck_type', 'required|callback_truck_type_check');
		$this->form_validation->set_rules('payload_size', 'payload_size', 'required');
		$this->form_validation->set_rules('payload_height', 'payload_height', 'required');
		$this->form_validation->set_rules('load_type', 'load_type', 'required');
		$this->form_validation->set_rules('pickup_point', 'pickup_point', 'required');
		$this->form_validation->set_rules('drop_point', 'drop_point', 'required');
		$this->form_validation->set_rules('pickup_lat', 'pickup_lat', 'required');
		$this->form_validation->set_rules('pickup_lng', 'pickup_lng', 'required');
		$this->form_validation->set_rules('dropoff_lat', 'dropoff_lat', 'required');
		$this->form_validation->set_rules('dropoff_lng', 'dropoff_lng', 'required');
		
		if ($this->form_validation->run() == FALSE)
        {
            $data['status']=false;
            $data['error'] =validation_errors_response(); 
        }   
        else
        {		
        		$request = $this->post();
        		// $find_km = getDistance($request['pickup_lat'],$request['pickup_lng'],$request['dropoff_lat'],$request['dropoff_lng']);
        		$find_km = 1500;
				$fare_data['km']= $find_km;
				$fare_data['truck_type']= $request['truck_type'];
				$fare_data['payload_size']=$request['payload_size'];
				$fare_data['payload_height']=$request['payload_height'];
				$fare_data['ban_time_hour']=0;
				$fare_data['ban_time_charge']=20;
				$fare_data['pickup_point']=$request['pickup_point'];
				$fare_data['drop_point']=$request['drop_point'];
				$fare_data['load_type']=$request['load_type'];
				$fare_data['geo_zone']=0;
				$fare_data['seasons']=0;
				$fare_data['pick_time']="";
				$fare_data['city_correction']="";
				$fare_data['credit']=0;
				if(get_fare($fare_data))
				{
					$data['status'] = true;
					$data['data'] = get_fare($fare_data);
				}
				else
				{
					$data['status'] = false;
					$data['error'] = "Invalid truck type!";	
				}
        }
        $this->response($data);
	}
	public function truck_type_check($str) {
       
        if ($str == '0' || $str == 0) {
            $this->form_validation->set_message('truck_type_check', 'The {field} field can not be 0');
            return FALSE;
        } else {
            return TRUE;
        }
    }
}