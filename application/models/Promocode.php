<?php defined('BASEPATH') OR exit('No direct script access allowed');
use \Illuminate\Database\Eloquent\Model as Eloquent;
class Promocode extends Eloquent{
    protected $table = 'promocode';
	public $timestamps = false;
    
}
