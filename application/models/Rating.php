<?php defined('BASEPATH') OR exit('No direct script access allowed');
use \Illuminate\Database\Eloquent\Model as Eloquent;
class Rating extends Eloquent{
    protected $table = 'rating';
	public $timestamps = false;
    
}
