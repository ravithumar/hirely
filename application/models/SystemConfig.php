<?php defined('BASEPATH') OR exit('No direct script access allowed');
use \Illuminate\Database\Eloquent\Model as Eloquent;
class SystemConfig extends Eloquent{
    protected $table = 'system_config';
	public $timestamps = false;
    
}
