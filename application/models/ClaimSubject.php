<?php defined('BASEPATH') OR exit('No direct script access allowed');
use \Illuminate\Database\Eloquent\Model as Eloquent;
class ClaimSubject extends Eloquent{
    protected $table = 'claim_subject';
	public $timestamps = false;
    
}
