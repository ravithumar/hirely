<?php defined('BASEPATH') OR exit('No direct script access allowed');
use \Illuminate\Database\Eloquent\Model as Eloquent;
class Roles extends Eloquent {
	protected $table = 'groups';
	public $timestamps = false;
	protected $appends = ['privilege'];
	public function getPrivilegeAttribute(){
		$CI =& get_instance();
		$CI->db->select('*');
		$CI->db->where('group_id', $this->id);
		$query = $CI->db->get('group_privilege');
		return $query->result_array();
	}
}
