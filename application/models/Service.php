<?php defined('BASEPATH') OR exit('No direct script access allowed');
use \Illuminate\Database\Eloquent\Model as Eloquent;
class Service extends Eloquent{
    protected $table = 'services';
	public $timestamps = false;	
	protected $hidden = ['deleted_at']; 
  
}
