<?php defined('BASEPATH') OR exit('No direct script access allowed');
use \Illuminate\Database\Eloquent\Model as Eloquent;
class Wallet extends Eloquent{
    protected $table = 'wallet';
	public $timestamps = false;
    
}
