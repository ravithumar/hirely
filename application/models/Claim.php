<?php defined('BASEPATH') OR exit('No direct script access allowed');
use \Illuminate\Database\Eloquent\Model as Eloquent;
class Claim extends Eloquent{
    protected $table = 'claim';
	public $timestamps = false;
    
}
