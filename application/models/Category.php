<?php defined('BASEPATH') OR exit('No direct script access allowed');
use \Illuminate\Database\Eloquent\Model as Eloquent;
class Category extends Eloquent{
    protected $table = 'category';
	public $timestamps = false;
	protected $appends = ['sub_category'];
	protected $hidden = ['deleted_at']; 

	public function getSubCategoryAttribute()
    {    	
    	$CI = &get_instance();
		$CI->load->model('SubCategory');
		return SubCategory::whereCategoryId($this->id)->get();
    }
    
}
