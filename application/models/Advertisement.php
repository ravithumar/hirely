<?php defined('BASEPATH') OR exit('No direct script access allowed');
use \Illuminate\Database\Eloquent\Model as Eloquent;
class Advertisement extends Eloquent{
    protected $table = 'advertisement';
	public $timestamps = false;
    
}
