<?php 
/**
 * summary
 */
// require APPPATH . '/libraries/REST_Controller.php';
abstract class MY_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('ion_auth');
        if (!$this->ion_auth->logged_in()) {
            redirect('auth/login');
        }  
    }
    public function render($page = null,$params = null, $return = false)
    {
        
        $params['body_class'] = $this->_generate_body_class();
        $this->load->view('front/layout/header',$params);
        if($page != null){
            $this->load->view('front/pages/'.$page,$params,$return);
        }
        $this->load->view('front/layout/footer',$params);
    }
    public function renderAdmin($page = null,$params = null, $return = false)
    {
        
        $params['body_class'] = $this->_generate_body_class();
        $this->load->view('admin/layout/header',$params);
        if($page != null){
            $this->load->view('admin/pages/'.$page,$params,$return);
        }
        $this->load->view('admin/layout/footer',$params);
    }
    public function _generate_body_class()
    {
        if($this->uri->segment_array() == null){
            $uri = array('index');
        }else{
            $uri = $this->uri->segment_array();
            if(end($uri) == 'index'){
                array_pop($uri);
            }
        }
        return implode('-', $uri);
    }
}
