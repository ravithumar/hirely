<!-- //menu section -->
<!-- body section -->
<section class="body-main-section">
   <!-- header section -->
   <div class="header-top-wrapper services-bg">
      <div class="container">
         <div class="row d-flex align-items-center">
            <div class="col-md-12 padtb150">
               <h1 class="text-center fontwebold">Our Services</h1>
            </div>
         </div>
      </div>
   </div>
   <!-- //header section -->
   <!-- best ervices section -->
   <div class="best-services-wrapper">
      <div class="container pt-5 pb-5">
         <div class="row">
            <div class="col-md-12">
               <p class="small-pink-font text-uppercase mb-0">best services</p>
               <h2 class="border-bottom-pink pb-3">What are our services</h2>
               <p class="pt-1">we have a wide array of services with flexible price ranges suited for all kinds of customers’ needs. Customer will be given a choice to choose between registered businesses or experienced freelancers. Customers can also filter options with their convenient price range, distance and rating. </p>
               <div class="row pt-5">
                  <div class="col-md-4">
                     <div class="grey-border">
                        <img src="<?php echo assets('front/images/recurring-cleaning.png'); ?>" class="" alt="recurring cleaning">
                        <h4 class="pt-4">Cleaning services</h4>
                        <p class="max-width500 pt-3 line-height25">This category includes house cleaning services, commercial cleaning services, air-con cleaning services, pool cleaning services, tiles washing services, sanitization and disinfection services, pressure washing services, curtain cleaning services, carpet cleaning services, window cleaning services, pest control and others.</p>
                     </div>
                  </div>
                  <div class="col-md-4 padtopxs20">
                     <div class="grey-border">
                        <img src="<?php echo assets('front/images/beauty-service.png'); ?>" class="" alt="Beauty Service">
                        <h4 class="pt-4">Beauty and wellness services</h4>
                        <p class="max-width500 pt-3 line-height25">This category includes facial services, grooming services, massage services and others</p>
                     </div>
                  </div>
                  <div class="col-md-4 padtopxs20">
                     <div class="grey-border">
                        <img src="<?php echo assets('front/images/repairing-service.png'); ?>" class="" alt="Repairing Service">
                        <h4 class="pt-4">Repair services</h4>
                        <p class="max-width500 pt-3 line-height25">This category includes handyman services, car repair services, air-con repair services, plumbing services, electrician services, carpentry, lock smith services, kitchen repair services and others.</p>
                     </div>
                  </div>
               </div>
               <div class="row padtop33">
                  <div class="col-md-4">
                     <div class="grey-border">
                        <img src="<?php echo assets('front/images/doctor-service.png'); ?>" class="" alt="recurring cleaning">
                        <h4 class="pt-4">Gastronomical Services</h4>
                        <p class="max-width500 pt-3 line-height25">This category includes cooking services and catering services for a wide range of cuisines </p>
                     </div>
                  </div>
                  <div class="col-md-4 padtopxs20">
                     <div class="grey-border">
                        <img src="<?php echo assets('front/images/beauty-service.png'); ?>" class="" alt="Beauty Service">
                        <h4 class="pt-4">Renovation services</h4>
                        <p class="max-width500 pt-3 line-height25">This category includes painting services, moving services, gardening services, flooring services and others </p>
                     </div>
                  </div>
                  <div class="col-md-4 padtopxs20">
                     <div class="grey-border">
                        <img src="<?php echo assets('front/images/beauty-service.png'); ?>" class="" alt="Beauty Service">
                        <h4 class="pt-4">Party services</h4>
                        <p class="max-width500 pt-3 line-height25">This category includes decoration services, entertainment services and others </p>
                     </div>
                  </div>
               </div>
               <div class="row padtop33">
                  <div class="col-md-4 padtopxs20">
                     <div class="grey-border">
                        <img src="<?php echo assets('front/images/beauty-service.png'); ?>" class="" alt="Beauty Service">
                        <h4 class="pt-4">Medical services</h4>
                        <p class="max-width500 pt-3 line-height25">This category includes house doctor services, special aid services and others </p>
                     </div>
                  </div>
                  <div class="col-md-4 padtopxs20">
                     <div class="grey-border">
                        <img src="<?php echo assets('front/images/beauty-service.png'); ?>" class="" alt="Beauty Service">
                        <h4 class="pt-4">Tutoring services</h4>
                        <p class="max-width500 pt-3 line-height25">This category includes tutoring services, both academic related and non academic related, for a wide range of standards and subjects. </p>
                     </div>
                  </div>
                  <div class="col-md-4 padtopxs20">
                     <div class="grey-border">
                        <img src="<?php echo assets('front/images/beauty-service.png'); ?>" class="" alt="Beauty Service">
                        <h4 class="pt-4">Pet services</h4>
                        <p class="max-width500 pt-3 line-height25">This category includes grooming services, pet health services, pet sitting services etc  </p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!--//best ervices section -->
   <!-- left image right text section -->
   <div class="left-img-right-text-wrapper">
      <div class="container pt-4 padbot160 padbotxs50">
         <div class="row d-flex align-items-center">
            <div class="col-md-6">
               <img src="<?php echo assets('front/images/team.png'); ?>" class="img img-fluid" alt="team">
            </div>
            <div class="col-md-6 pr-md-5 pl-lg-0 padtopxs20 padleft10ipad">
               <p class="small-pink-font text-uppercase mb-2">Quality network of service providers</p>
               <h2 class="border-bottom-pink pb-3">Quality network of service providers</h2>
               <p class="max-width500">Our open network exposes customers to a large variety of service providers. This helps the customer to narrow down a suitable service provider to address their specific </p>
            </div>
         </div>
      </div>
   </div>
   <!-- //left image right text section -->
   <!-- pink mobile app -->
   <div class="mobile-app background-pink">
      <div class="container">
         <div class="row d-flex align-items-center">
            <div class="col-md-6 padtbipad50">
               <div class="max-width400">
                  <h2 class="font-white fontwbold padtopxs50 font45">Get The App
                     Free&nbsp;From Here
                  </h2>
                  <p class="font-white pt-4">Get our app to enjoy exclusive discounts and offers
                  </p>
                  <div class="row pt-3">
                     <div class="col-md-6 col-lg-6 pr-md-0 col-12">
                        <a href="#" class="blackbox d-flex align-items-center">
                           <img src="<?php echo assets('front/images/playstore-icon.png'); ?>" width="35" alt="playstore" class="playstore-icon">
                           <p class="font13 pl-3 mb-0">
                              <span class="fontw300">  Coming soon on</span><br>
                              <span class="fontw600">Google Playstore</span>
                           </p>
                        </a>
                     </div>
                     <div class="col-md-6 col-lg-6 pr-md-0 col-12 pl-4 padtopxs20 padleft15xs">
                        <a href="#" class="blackbox d-flex align-items-center">
                           <img src="<?php echo assets('front/images/apple-icon.png'); ?>" width="30" alt="apple" class="playstore-icon">
                           <p class="font13 pl-3 mb-0">
                              <span class="fontw300">Coming soon on</span><br>
                              <span class="fontw600">App Store</span>
                           </p>
                        </a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-6">
               <img src="<?php echo assets('front/images/mobile-pink-app.png'); ?>" class="img img-fluid mtb85" alt="mobile app">
            </div>
         </div>
      </div>
   </div>
   <div class="padbot160"></div>
   <!-- //pink mobile app -->   
</section>
<!-- //body section -->
<!-- footer section -->
