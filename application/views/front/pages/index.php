<!-- body section -->
<section class="body-main-section">
   <!-- header section -->
   <div class="header-top-wrapper background-dark-pink">
      <div class="container">
         <div class="row d-flex align-items-center">
            <div class="col-md-6 padtopxs20 padtbipad50">
               <!-- <h1>Complete <span class="font-pink">Cleaning</span> And <span class="font-pink">Disinfection</span> For Your&nbsp;<span class="font-pink">Home</span></h1> -->
               <h1>One click away to hire a <span class="font-pink">service at your</span> doorstep</h1>
               <p class="pt-3 max-width500 line-height35">We are a new-fashioned marketplace app dedicated to bring all kinds of home based services together in one platform for your convenience
               </p>
               <div class="row pt-3">
                  <div class="col-md-6 col-lg-4 pr-md-0 col-10 padleft0pro">
                     <a href="#" class="blackbox d-flex align-items-center">
                        <img src="<?php echo assets('front/images/playstore-icon.png'); ?>" width="35" alt="playstore" class="playstore-icon">
                        <p class="font13 pl-3 mb-0">
                           <span class="fontw300"> Coming soon on</span><br>
                           <span class="fontw600">Google Playstore</span>
                        </p>
                     </a>
                  </div>
                  <div class="col-md-6 col-lg-4 pr-md-0 col-10 pl-md-4 padtopxs20">
                     <a href="#" class="blackbox d-flex align-items-center">
                        <img src="<?php echo assets('front/images/apple-icon.png'); ?>" width="30" alt="apple" class="playstore-icon">
                        <p class="font13 pl-3 mb-0">
                           <span class="fontw300">Coming soon on</span><br>
                           <span class="fontw600">App Store</span>
                        </p>
                     </a>
                  </div>
               </div>
            </div>
            <div class="col-md-6 padtopxs20">
               <img src="<?php echo assets('front/images/header-mobile-img.png'); ?>" class="img img-fluid" class="mobile app">
            </div>
         </div>
      </div>
   </div>
   <!-- //header section -->
   <!-- best ervices section -->
   <div class="best-services-wrapper">
      <div class="container pt-5 pb-5">
         <div class="row">
            <div class="col-md-12">
               <p class="small-pink-font text-uppercase mb-0">best services</p>
               <h2 class="border-bottom-pink pb-3">What are our services</h2>
               <div class="row">
                  <div class="col-md-6">
                     <p class="max-width500">we have a wide array of services with flexible price ranges suited for all kinds of customers’ needs. Customer will be given a choice to choose between registered businesses or experienced freelancers. Customers can also filter options with their convenient price range, distance and rating. </p>
                  </div>
                  <div class="col-md-6">
                     <div class="pink-button-box d-flex">
                        <a href="<?php echo ('services'); ?>"><button type="button" class="pink-button d-flex align-items-center">See More</button></a>
                     </div>
                  </div>
               </div>
               <div class="row pt-5">
                  <div class="col-md-4">
                     <div class="grey-border">
                        <img src="<?php echo assets('front/images/recurring-cleaning.png'); ?>" class="" alt="recurring cleaning">
                        <h4 class="pt-4">Cleaning services</h4>
                        <p class="max-width500 pt-3 line-height25">This category includes house cleaning services, commercial cleaning services, air-con cleaning services, pool cleaning services, tiles washing services, sanitization and disinfection services, pressure washing services, curtain cleaning services, carpet cleaning services, window cleaning services, pest control and others.</p>
                     </div>
                  </div>
                  <div class="col-md-4 padtopxs20">
                     <div class="grey-border">
                        <img src="<?php echo assets('front/images/beauty-service.png'); ?>" class="" alt="Beauty Service">
                        <h4 class="pt-4">Beauty and wellness services</h4>
                        <p class="max-width500 pt-3 line-height25">This category includes facial services, grooming services, massage services and others</p>
                     </div>
                  </div>
                  <div class="col-md-4 padtopxs20">
                     <div class="grey-border">
                        <img src="<?php echo assets('front/images/repairing-service.png'); ?>" class="" alt="Repairing Service">
                        <h4 class="pt-4">Repair services</h4>
                        <p class="max-width500 pt-3 line-height25">This category includes handyman services, car repair services, air-con repair services, plumbing services, electrician services, carpentry, lock smith services, kitchen repair services and others.</p>
                     </div>
                  </div>
               </div>
               <div class="row padtop33">
                  <div class="col-md-4">
                     <div class="grey-border">
                        <img src="<?php echo assets('front/images/doctor-service.png'); ?>" class="" alt="recurring cleaning">
                        <h4 class="pt-4">Gastronomical Services</h4>
                        <p class="max-width500 pt-3 line-height25">This category includes cooking services and catering services for a wide range of cuisines </p>
                     </div>
                  </div>
                  <div class="col-md-4 padtopxs20">
                     <div class="grey-border">
                        <img src="<?php echo assets('front/images/beauty-service.png'); ?>" class="" alt="Beauty Service">
                        <h4 class="pt-4">Renovation services</h4>
                        <p class="max-width500 pt-3 line-height25">This category includes painting services, moving services, gardening services, flooring services and others </p>
                     </div>
                  </div>
                  <div class="col-md-4 padtopxs20">
                     <div class="grey-border">
                        <img src="<?php echo assets('front/images/beauty-service.png'); ?>" class="" alt="Beauty Service">
                        <h4 class="pt-4">Party services</h4>
                        <p class="max-width500 pt-3 line-height25">This category includes decoration services, entertainment services and others </p>
                     </div>
                  </div>
               </div>
               <div class="row padtop33">
                  <div class="col-md-4 padtopxs20">
                     <div class="grey-border">
                        <img src="<?php echo assets('front/images/beauty-service.png'); ?>" class="" alt="Beauty Service">
                        <h4 class="pt-4">Medical services</h4>
                        <p class="max-width500 pt-3 line-height25">This category includes house doctor services, special aid services and others </p>
                     </div>
                  </div>
                  <div class="col-md-4 padtopxs20">
                     <div class="grey-border">
                        <img src="<?php echo assets('front/images/beauty-service.png'); ?>" class="" alt="Beauty Service">
                        <h4 class="pt-4">Tutoring services</h4>
                        <p class="max-width500 pt-3 line-height25">This category includes tutoring services, both academic related and non academic related, for a wide range of standards and subjects. </p>
                     </div>
                  </div>
                  <div class="col-md-4 padtopxs20">
                     <div class="grey-border">
                        <img src="<?php echo assets('front/images/beauty-service.png'); ?>" class="" alt="Beauty Service">
                        <h4 class="pt-4">Pet services</h4>
                        <p class="max-width500 pt-3 line-height25">This category includes grooming services, pet health services, pet sitting services etc  </p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!--//best ervices section -->
   <!-- left image right text section -->
   <div class="left-img-right-text-wrapper">
      <div class="container pt-4 pb-5">
         <div class="row d-flex align-items-center">
            <div class="col-md-6">
               <img src="<?php echo assets('front/images/cleaning-persons.png'); ?>" class="img img-fluid" alt="cleaning-persons">
            </div>
            <div class="col-md-6 pr-md-5 pl-lg-0 padtopxs20 padleft10ipad">
               <p class="small-pink-font text-uppercase mb-2">Quick and easy way to hire a serviceman</p>
               <h2 class="border-bottom-pink pb-3">Quick and easy way to hire a serviceman</h2>
               <p class="max-width500">Our app allows you to have a hassle free transaction. If you like a service in our app, just save, chat, book and repeat. You can also collect points and claim rewards. </p>
               <div class="pink-button-box mt-5">
                  <button type="button" class="pink-button d-flex align-items-center">Learn More</button>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- //left image right text section -->
   <!-- right image left text section -->
   <div class="right-img-left-text-wrapper">
      <div class="container pt-5 padbot160 padtop0xs padbotxs50">
         <div class="row d-flex align-items-center row-reverse">
            <div class="col-md-6 padtopxs20">
               <p class="small-pink-font text-uppercase mb-2">Reliable and trustworthy team</p>
               <h2 class="border-bottom-pink pb-3 max-width500">Reliable and trustworthy team</h2>
               <p class="max-width500">We value our customers and we understand their importance. Our team is dedicated to provide the best customer service possible. We are open to answering all your queries and will do our best to resolve your issue.   </p>
               <div class="pink-button-box mt-5">
                  <button type="button" class="pink-button d-flex align-items-center">Learn More</button>
               </div>
            </div>
            <div class="col-md-6">
               <img src="<?php echo assets('front/images/person-img.png'); ?>" class="img img-fluid" alt="person-img">
            </div>
         </div>
      </div>
   </div>
   <!-- //right image left text section -->
   <!-- pink mobile app -->
   <div class="mobile-app background-pink">
      <div class="container">
         <div class="row d-flex align-items-center">
            <div class="col-md-6 padtbipad50">
               <div class="max-width400">
                  <h2 class="font-white fontwbold padtopxs50 font45">Get The App
                     Free&nbsp;From Here
                  </h2>
                  <p class="font-white pt-4">Get our app to enjoy exclusive discounts and offers
                  </p>
                  <div class="row pt-3">
                     <div class="col-md-6 col-lg-6 pr-md-0 col-12">
                        <a href="#" class="blackbox d-flex align-items-center">
                           <img src="<?php echo assets('front/images/playstore-icon.png'); ?>" width="35" alt="playstore" class="playstore-icon">
                           <p class="font13 pl-3 mb-0">
                              <span class="fontw300">  Coming soon on</span><br>
                              <span class="fontw600">Google Playstore</span>
                           </p>
                        </a>
                     </div>
                     <div class="col-md-6 col-lg-6 pr-md-0 col-12 pl-4 padtopxs20 padleft15xs">
                        <a href="#" class="blackbox d-flex align-items-center">
                           <img src="<?php echo assets('front/images/apple-icon.png'); ?>" width="30" alt="apple" class="playstore-icon">
                           <p class="font13 pl-3 mb-0">
                              <span class="fontw300">Coming soon on</span><br>
                              <span class="fontw600">App Store</span>
                           </p>
                        </a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-6">
               <img src="<?php echo assets('front/images/mobile-pink-app.png'); ?>" class="img img-fluid mtb85" alt="mobile app">
            </div>
         </div>
      </div>
   </div>
   <div class="padbot160"></div>
   <!-- //pink mobile app -->
   <!-- testimonial section -->
   <div class="testimonial" style="display: none;">
      <div class="container pb-5">
         <div class="row d-flex align-items-center">
            <div class="col-md-12 padtopxs20">
               <p class="small-pink-font text-uppercase mb-2">Lorem Ipsum is simply dummy text</p>
               <h2 class="border-bottom-pink pb-3">What Our Clients' Say</h2>
               <p class="max-width400 line-height25">This is what clients have been saying after using the great service we do for clients.</p>
            </div>
         </div>
      </div>
   </div>
   <div class="testimonial-slider slider container-fluid" style="display: none;">
      <div>
         <div class="grey-border-box">
            <img src="<?php echo assets('front/images/quote.png'); ?>" class="mrtopm22" alt="quote">
            <p class="line-height25 pt-4 pb-4 font14-sm">“I love that I can spend more time with my husband, children, and family, and less time stressing over getting my house clean.”</p>
            <div class="row border-top-grey pt-4">
               <div class="col-md-12 col-lg-8 d-flex align-items-center padleft0ipad">
                  <img src="<?php echo assets('front/images/braun-yes.png'); ?>">
                  <div class="testimonial-name">
                     <p class="font17 mb-0">Nazmi Javier<br><span class="font14">Househusband</span></p>
                  </div>
               </div>
               <div class="col-md-12 col-lg-4 pl-md-0 mt-4">
                  <img src="<?php echo assets('front/images/star-rating.png'); ?>" class="img img-fluid" alt="5 star">
               </div>
            </div>
         </div>
      </div>
      <div>
         <div class="grey-border-box">
            <img src="<?php echo assets('front/images/quote.png'); ?>" class="mrtopm22" alt="quote">
            <p class="line-height25 pt-4 pb-4 font14-sm">“This is by far the simplest, most frictionless, easiest-to-get-going platform that I’ve ever worked in. Just being able to upload a link and bounce”</p>
            <div class="row border-top-grey pt-4">
               <div class="col-md-12 col-lg-8 d-flex align-items-center padleft0ipad">
                  <img src="<?php echo assets('front/images/chintya-xin.png'); ?>">
                  <div class="testimonial-name">
                     <p class="font17 mb-0">Chintya Xin<br><span class="font14">Housewife</span></p>
                  </div>
               </div>
               <div class="col-md-12 col-lg-4 pl-md-0 mt-4">
                  <img src="<?php echo assets('front/images/star-rating.png'); ?>" class="img img-fluid" alt="5 star">
               </div>
            </div>
         </div>
      </div>
      <div>
         <div class="grey-border-box">
            <img src="<?php echo assets('front/images/quote.png'); ?>" class="mrtopm22" alt="quote">
            <p class="line-height25 pt-4 pb-4 font14-sm">“This is by far the simplest, most frictionless, easiest-to-get-going platform that I’ve ever worked in. Just being able to upload a link and bounce”</p>
            <div class="row border-top-grey pt-4">
               <div class="col-md-12 col-lg-8 d-flex align-items-center padleft0ipad">
                  <img src="<?php echo assets('front/images/nazmi-jaview.png'); ?>">
                  <div class="testimonial-name">
                     <p class="font17 mb-0">Braun Yes<br><span class="font14">CEO Of Arcane</span></p>
                  </div>
               </div>
               <div class="col-md-12 col-lg-4 pl-md-0 mt-4">
                  <img src="<?php echo assets('front/images/star-rating.png'); ?>" class="img img-fluid" alt="5 star">
               </div>
            </div>
         </div>
      </div>
      <div>
         <div class="grey-border-box">
            <img src="<?php echo assets('front/images/quote.png'); ?>" class="mrtopm22" alt="quote">
            <p class="line-height25 pt-4 pb-4 font14-sm">“This is by far the simplest, most frictionless, easiest-to-get-going platform that I’ve ever worked in. Just being able to upload a link and bounce”</p>
            <div class="row border-top-grey pt-4">
               <div class="col-md-12 col-lg-8 d-flex align-items-center padleft0ipad">
                  <img src="<?php echo assets('front/images/nazmi-jaview.png'); ?>">
                  <div class="testimonial-name">
                     <p class="font17 mb-0">Braun Yes<br><span class="font14">CEO Of Arcane</span></p>
                  </div>
               </div>
               <div class="col-md-12 col-lg-4 pl-md-0 mt-4">
                  <img src="<?php echo assets('front/images/star-rating.png'); ?>" class="img img-fluid" alt="5 star">
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- //testimonial section -->  
</section>
<!-- //body section -->
