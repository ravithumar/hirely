<!-- body section -->
<section class="body-main-section">
   <!-- header section -->
   <div class="header-top-wrapper about-bg">
      <div class="container">
         <div class="row d-flex align-items-center">
            <div class="col-md-12 padtb150">
               <h1 class="text-center fontwebold">About Us</h1>
            </div>
         </div>
      </div>
   </div>
   <!-- //header section -->
   <!-- why choose section -->
   <div class="why-choose-wrapper">
      <div class="container pt-5 pb-5">
         <div class="row pt-3">
            <div class="col-md-12">
               <h2 class="pb-5">Why Choose Us</h2>
            </div>
         </div>
         <div class="row pt-5">
            <div class="col-md-6 pr-5">
               <div class="row">
                  <div class="col-md-2 pl-0 pr-md-0 col-3">
                     <img src="<?php echo assets('front/images/arrow-round-right.png'); ?>" class="img img-fluid" alt="right arrow">
                  </div>
                  <div class="col-md-10 pl-3 col-9 padleftxs0">
                     <div class=" border-top-grey">
                        <h4 class="pt-4">Vivamus lectus arcu?</h4>
                        <p class="pt-3">1.We are a Singapore based app Being local based, we are aware of Singaporean’s concerns. Our app is tailor made for the convenience of both locals and expats living in Singapore. We strive to make your lives easier.</p>
                     </div>
                  </div>
               </div>
               <div class="row pt-4">
                  <div class="col-md-2 pl-0 pr-md-0 col-3">
                     <img src="<?php echo assets('front/images/arrow-round-right.png'); ?>" class="img img-fluid" alt="right arrow">
                  </div>
                  <div class="col-md-10 pl-3 col-9 padleftxs0">
                     <div class=" border-top-grey">
                        <h4 class="pt-4">Vivamus lectus arcu?</h4>
                        <p class="pt-3">We believe in diversity Singapore is a melting pot of cultures and races. We hope to implement the beauty in diversity of Singapore into our app. We have an open source of networking for service providers to showcase their business through our app and our marketing platforms. You will never be short of options</p>
                     </div>
                  </div>
               </div>
               <div class="row pt-4">
                  <div class="col-md-2 pl-0 pr-md-0 col-3">
                     <img src="<?php echo assets('front/images/arrow-round-right.png'); ?>" class="img img-fluid" alt="right arrow">
                  </div>
                  <div class="col-md-10 pl-3 col-9 padleftxs0">
                     <div class=" border-top-grey">
                        <h4 class="pt-4">Vivamus lectus arcu?</h4>
                        <p class="pt-3">We value our customers the most. We believe that our customers are our biggest asset. We believe in building a long term dependable relationship with our customers. We guarantee to be of as much help as possible. We take every complaint seriously and will assist in refund if we see fit.  </p>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-6">
               <img src="<?php echo assets('front/images/team-group.png'); ?>" class="img img-fluid" alt="team group">
            </div>
         </div>
      </div>
   </div>
   <!--//why choose section -->
   <!-- video section -->
   <div class="video-wrapper">
      <div class="container pt-5 pb-5">
         <div class="row justify-content-center">
            <div class="col-md-8 px-md-0">
               <div class="hp-about-sciprep-video">
                  <div class="wrapper">
                     <img  id="video-cover" src="<?php echo assets('front/images/video-img.png'); ?>" alt="Video title">
                     <iframe id="player" width="100%" height="100%" src="https://www.youtube.com/embed/yAoLSRbwxL8" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                     <button id="play" class="play-btn">
                     <img src="<?php echo assets('front/'); ?>images/play-button-img.png" alt="">
                     </button>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- //video section -->
   <!-- right image left text section -->
   <div class="right-img-left-text-wrapper">
      <div class="container pt-5 padbot160 padtop0xs padbotxs50">
         <div class="row d-flex align-items-center row-reverse">
            <div class="col-md-6 padtopxs20">
               <p class="small-pink-font text-uppercase mb-2">Lorem Ipsum is simply dummy text</p>
               <h2 class="border-bottom-pink pb-3 max-width500">Lorem Ipsum is simply dummy text of the</h2>
               <p class="max-width500">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took   </p>
               <div class="pink-button-box mt-5">
                  <button type="button" class="pink-button d-flex align-items-center">Learn More</button>
               </div>
            </div>
            <div class="col-md-6">
               <img src="<?php echo assets('front/images/person-img.png'); ?>" class="img img-fluid" alt="person-img">
            </div>
         </div>
      </div>
   </div>
   <!-- //right image left text section -->
   <!-- pink mobile app -->
   <div class="mobile-app background-pink">
      <div class="container">
         <div class="row d-flex align-items-center">
            <div class="col-md-6 padtbipad50">
               <div class="max-width400">
                  <h2 class="font-white fontwbold padtopxs50 font45">Get The App
                     Free&nbsp;From Here
                  </h2>
                  <p class="font-white pt-4">Get our app to enjoy exclusive discounts and offers
                  </p>
                  <div class="row pt-3">
                     <div class="col-md-6 col-lg-6 pr-md-0 col-12">
                        <a href="#" class="blackbox d-flex align-items-center">
                           <img src="<?php echo assets('front/images/playstore-icon.png'); ?>" width="35" alt="playstore" class="playstore-icon">
                           <p class="font13 pl-3 mb-0">
                              <span class="fontw300">  Coming soon on</span><br>
                              <span class="fontw600">Google Playstore</span>
                           </p>
                        </a>
                     </div>
                     <div class="col-md-6 col-lg-6 pr-md-0 col-12 pl-4 padtopxs20 padleft15xs">
                        <a href="#" class="blackbox d-flex align-items-center">
                           <img src="<?php echo assets('front/images/apple-icon.png'); ?>" width="30" alt="apple" class="playstore-icon">
                           <p class="font13 pl-3 mb-0">
                              <span class="fontw300">Coming soon on</span><br>
                              <span class="fontw600">App Store</span>
                           </p>
                        </a>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-6">
               <img src="<?php echo assets('front/images/mobile-pink-app.png'); ?>" class="img img-fluid mtb85" alt="mobile app">
            </div>
         </div>
      </div>
   </div>
   <div class="padbot160"></div>
   <!-- //pink mobile app -->   
</section>
<!-- //body section -->
