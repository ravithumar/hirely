<!-- body section -->
<section class="body-main-section">
   <!-- header section -->
   <div class="header-top-wrapper written-bg">
      <div class="container">
         <div class="row d-flex align-items-center">
            <div class="col-md-12 padtb150">
               <h1 class="text-center fontwebold">Privacy Policy</h1>
            </div>
         </div>
      </div>
   </div>
   <!-- //header section -->
   <!-- text section -->
   <div class="text-wrapper">
      <div class="container padtop100 pb-5">
         <div class="row">
            <div class="col-md-12">
               <?php echo $privacy_policy->value; ?>
            </div>
         </div>
      </div>
   </div>
   <!-- //text section -->
</section>
<!-- //body section -->
