<div class="left-side-menu">

    <div class="slimscroll-menu">

        <!--- Sidemenu -->

        <div id="sidebar-menu">

            <ul class="metismenu" id="side-menu">

                <li>

                    <a href="<?php echo base_url('admin'); ?>" class="waves-effect"><i class="ti-home"></i> <span> Dashboard </span></a>

                </li>

                <li>

                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-user"></i><span>Users Management </span><span class="menu-arrow"></span></a>

                    <ul class="nav-second-level" aria-expanded="false">

                        <li>

                            <a href="<?php echo routeLink('admin/users') ?>" class="waves-effect"><span>- Users</span></a>

                        </li>

                        <li>

                            <a href="<?php echo routeLink('admin/service-provider') ?>" class="waves-effect"><span>- Service Provider </span></a>

                        </li>

                    </ul>

                </li>

                <li>

                    <a href="javascript:void(0);" class="waves-effect"><i class="fas fa-tasks"></i> <span>Service Management</span><span class="menu-arrow"></span></a>

                    <ul class="nav-second-level" aria-expanded="false">

                        <li>

                            <a href="<?php echo base_url('admin/category'); ?>" class="waves-effect"> <span>- Category</span></a>

                        </li>

                        <li>

                            <a href="<?php echo base_url('admin/sub-category'); ?>" class="waves-effect"> <span>- Sub Category</span></a>

                        </li>

                        <li>

                            <a href="<?php echo base_url('admin/service'); ?>" class="waves-effect"><span>- Serveices</span></a>

                        </li>

                    </ul>

                </li>

                <li>

                    <a href="javascript:void(0);" class="waves-effect"><i class="fa fa-exchange"></i> <span> Claims Management </span><span class="menu-arrow"></span></a>

                    <ul class="nav-second-level" aria-expanded="false">

                        <li>

                            <a href="<?php echo base_url('admin/claim-subject'); ?>" class="waves-effect"><span>- Claims Subjects </span></a>

                        </li>

                        <li>

                            <a href="<?php echo base_url('admin/claim'); ?>" class="waves-effect"><span>- Claims </span></a>

                        </li>

                    </ul>

                </li>

                <li>

                    <a href="<?php echo base_url('admin/promocode'); ?>" class="waves-effect"><i class="fa fa-code"></i> <span> Promocodes </span></a>

                </li>

                <li>

                    <a href="<?php echo base_url('admin/advertisement'); ?>" class="waves-effect"><i class="fas fa-ad"></i> <span> Advertisement </span></a>

                </li>

                <li>

                    <a href="javascript:void(0);" class="waves-effect"><i class="fas fa-file"></i> <span>Report</span><span class="menu-arrow"></span></a>

                    <ul class="nav-second-level" aria-expanded="false">

                        <li>

                            <a href="<?php echo base_url('admin/sub-report'); ?>" class="waves-effect"> <span>- Sub report 1</span></a>

                        </li>

                        <li>

                            <a href="<?php echo base_url('admin/sub-report'); ?>" class="waves-effect"> <span>- Sub report 2</span></a>

                        </li>

                        <li>

                            <a href="<?php echo base_url('admin/sub-report'); ?>" class="waves-effect"><span>- Sub report 3</span></a>

                        </li>

                    </ul>

                </li>

                <li>

                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-settings"></i> <span> <?php echo __('Configuration') ?> </span><span class="menu-arrow"></span></a>

                    <ul class="nav-second-level" aria-expanded="false">

                        <li>

                            <a href="<?php echo base_url('admin/system/setting'); ?>" class="waves-effect"><span> <?php echo __('System') ?> </span></a>

                        </li>

                        <li>

                            <a href="<?php echo base_url('admin/app/setting'); ?>" class="waves-effect"><span> <?php echo __('Application') ?> </span></a>

                        </li>

                    </ul>

                </li>

            </ul>

        </div>

        <!-- End Sidebar -->

        <div class="clearfix"></div>

    </div>

    <!-- Sidebar -left -->

</div>

<div class="content-page">