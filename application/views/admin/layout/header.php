<!DOCTYPE html>

<html lang="en">

    <head>

        <meta charset="utf-8" />

        <meta charset="utf-8">

        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>

        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />

         <title><?php echo config('site_title'); ?></title>

        <meta name="description" content="<?php echo config('site_meta'); ?>">

        <meta name="keyword" content="<?php echo config('site_keyword'); ?>">

        <link rel="shortcut icon" href="<?php echo base_url('assets/images/favicon.png') ?>">

        <!-- Plugins css -->

        <link href="<?php echo assets('v3/libs/flatpickr/flatpickr.min.css');?>" rel="stylesheet" type="text/css" />
        
        <link href="<?php echo assets('v3/libs/ladda/ladda-themeless.min.css');?>" rel="stylesheet" type="text/css" />

        <!-- Font awesome -->

        <link href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome.min.css"rel="stylesheet">

        <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"rel="stylesheet">

        <!-- App css -->

        <link href="<?php echo assets('v3/css/bootstrap.min.css');?>" rel="stylesheet" type="text/css" />

        <link href="<?php echo assets('v3/css/icons.min.css');?>" rel="stylesheet" type="text/css" />

        <link href="<?php echo assets('v3/css/app.min.css');?>" rel="stylesheet" type="text/css" />

        <link href="<?php echo assets('v3/css/style.css');?>" rel="stylesheet" type="text/css" />        

         <script src="<?php echo assets('js/jquery.min.js');?>"></script>

         <link href="<?php echo assets('plugins/select2/select2.min.css'); ?>" rel="stylesheet" type="text/css">
         
        <link href="<?php echo assets('v3/libs/sweetalert2/sweetalert2.min.css');?>" rel="stylesheet" type="text/css" />
        
        <link href="<?php echo assets('v3/libs/bootstrap-datepicker/bootstrap-datepicker.min.css');?>" rel="stylesheet" type="text/css" />

        <link href="<?php echo assets('v3/libs/summernote/summernote-bs4.css');?>" rel="stylesheet" type="text/css" />

         <link rel="stylesheet" href="<?php echo assets('plugins/magnific-popup/magnific-popup.css');?>"/>         

         <script type="text/javascript"> var BASE_URL = '<?php echo base_url(); ?>'; </script>

    </head>

    <body>

        <body class="fixed-left">

            <div id="wrapper">

                <!-- Topbar Start -->

                <div class="navbar-custom">

                    <ul class="list-unstyled topnav-menu float-right mb-0">

                        <li class="dropdown notification-list">

                            <a class="nav-link dropdown-toggle nav-user mr-0 waves-effect waves-light" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">

                                <img src="<?php echo assets('images/avtar.png');?>" alt="user-image" class="rounded-circle">

                                <span class="pro-user-name ml-1">

                                   <i class="mdi mdi-chevron-down"></i>

                                </span>

                            </a>

                            <div class="dropdown-menu dropdown-menu-right profile-dropdown ">

                                <!-- item-->

                                <div class="dropdown-header noti-title">

                                    <h6 class="text-overflow m-0">Welcome ! <?php echo $this->session->userdata('admin')['name'];?> </h6>

                                </div>

                                <a href="javascript:void(0);" class="dropdown-item notify-item" data-toggle="modal" data-target="#myModal">

                                    <i class="md  md-vpn-key"></i> <span><?php echo __('Change password') ?></span>

                                </a>

                                <a href="<?php echo base_url('auth/logout/admin'); ?>" class="dropdown-item notify-item">

                                    <i class="md md-settings-power"></i> <span>Logout</span>

                                </a>

                            </div>

                        </li>

                        

                    </ul>

                    <!-- LOGO -->

                    <div class="logo-box">

                        <a href="index.html" class="logo text-center">

                            <span class="logo-lg">

                                <span class="logo-lg-text-light">HIRELY</span>

                            </span>

                            <span class="logo-sm">

                                <span class="logo-lg-text-light">H</span>

                                

                            </span>

                        </a>

                    </div>

                    <ul class="list-unstyled topnav-menu topnav-menu-left m-0">

                        <li>

                            <button class="button-menu-mobile waves-effect waves-light">

                            <i class="fe-menu"></i>

                            </button>

                        </li>

                        

                    </ul>

                </div>

                <?php $this->load->view('admin/layout/sidebar'); ?>

              