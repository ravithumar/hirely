<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="page-title-box">
               <h4 class="page-title"><?php echo $main_title; ?></h4>
               <ol class="breadcrumb m-0">
                  <li class="breadcrumb-item"><a href="<?php echo $home; ?>"><?php echo $main_title; ?></a></li>
                  <li class="breadcrumb-item"><a href="javascript: void(0);">Edit</a></li>
               </ol>
            </div>
         </div>
      </div>
      <!-- Page-Title -->      
      <?php $this->load->view('admin/includes/message'); ?>
      <div class="row">
         <div class="col-sm-6">
            <form id="advertisement_add" enctype="multipart/form-data" action="<?php echo base_url('admin/advertisement/edit/'.$advertisement->id); ?>" method="post">
               <div class="portlet ">
                  <div class="portlet-heading clearfix">
                     <h3 class="portlet-title">
                        <?php echo $title; ?>
                     </h3>
                     <div class="portlet-widgets">
                        <a data-toggle="collapse" data-parent="#accordion1" href="#tab-email"><i class="ion-minus-round"></i></a>
                     </div>
                  </div>
                  <div id="tab-email" class="panel-collapse collapse show">
                     <div class="portlet-body">
                        <div class="col-12 col-md-12">
                           <div class="form-group">
                              <label>Title</label><br />
                              <fieldset class="form-group form-group-style">                                  
                                 <input type="text" id="" class="form-control " name="title"  required="" data-parsley-required-message="Please Enter Title" placeholder="Please Enter Title" data-parsley-errors-container="#from_date_error" value="<?php echo $advertisement->title; ?>">
                              </fieldset>
                              <div id="from_date_error"></div>
                           </div>
                           <div class="form-group">
                              <label>Description</label><br />
                              <fieldset class="form-group form-group-style">
                                <textarea id=""  class="form-control " name="description"  required="" data-parsley-required-message="Please Enter Description" placeholder="Please Enter Description" data-parsley-errors-container="#to_date_error " style="height: 200px;"><?php echo $advertisement->description; ?></textarea>
                              </fieldset>
                              <div id="to_date_error"></div>
                           </div>                           
                           <div class="form-group">
                              <label>Redirect Url</label><br />                              
                               <input type="url" id="" class="form-control " name="redirect_url"  required="" data-parsley-required-message="Please Enter Redirect Url" placeholder="Please Enter Redirect Url" data-parsley-errors-container="#redict_url_error" value="<?php echo $advertisement->redirect_url; ?>">
                               <div id="redict_url_error"></div>
                           </div>
                           <div class="form-group">
                              <label class="form-control-label">Image</label>
                              <div class="custom-file">
                                 <input type="file" name="image" id="file" class="custom-file-input parsley-error advertisement_image" id="advertisement_image" data-parsley-required-message="Please Select Image" data-parsley-errors-container="#advertisement_image_error" accept="image/*" id="form_image" data-parsley-fileextension="jpg||png||jpeg||JPG||PNG||JPEG"  >
                                 <label class="custom-file-label" >Choose file</label>
                              </div>
                              <div class="" id="advertisement_image_preview" style="margin-top: 5px;"> </div>
                              <div class="col-12 mt-2">
                                 <?php
                                    if (isset($advertisement['image']) && $advertisement['image'] != '')
                                    {
                                        if (file_exists($advertisement['image']))
                                        {
                                            $img_src = BASE_URL() . $advertisement['image'];
                                            $dis_attr = '';
                                            $image_class = "d-block";
                                        }
                                        else
                                        {
                                            $img_src = BASE_URL() . '/assets/images/default-profile.png';
                                            $dis_attr = 'disabled ';
                                            $image_class = "";
                                        }
                                    
                                    }
                                    else
                                    {
                                        $img_src = BASE_URL() . '/assets/images/default-profile.png';
                                        $dis_attr = 'disabled ';
                                        $image_class = "";
                                    }
                                    ?>
                                 <a href="<?php echo $img_src; ?>" class="image-popup">
                                 <img onerror="this.src='<?php echo base_url('assets/images/default-profile.png') ?>'" src="<?php echo $img_src; ?>" class="border rounded view_advertisement_image_preview preview  <?php echo $image_class; ?>" alt="Category" id="" style="height: auto;max-width: 100px;" >  
                                 </a>
                                 <span class="advertisement_image_error" id="advertisement_image_error" style="color: red;"></span>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-12">
                              <div class="page-title-box">
                                 <div class="page-title-right">
                                    <input type="button" onclick="$('#advertisement_add').submit();" class="btn btn-default pull-right mt-2 mb-2" value="<?php echo __('Save'); ?>">
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
<script type = "text/javascript" >
$(document).on("change", "#file", function() {
    var outputdata = [];
    var _URL = window.URL || window.webkitURL;
    // console.log(_URL);
    var fileSelect = document.getElementById('file');
    var files = fileSelect.files;
    var $this = $(this);
    var file = $(this)[0].files[0];

    img = new Image();
    var imgwidth = 0;
    var imgheight = 0;

    var width = 350;
    var height = 100;

    img.src = _URL.createObjectURL(file);
    
    var fileName = fileSelect.value,
    idxDot = fileName.lastIndexOf(".") + 1,
    extFile = fileName.substr(idxDot, fileName.length).toLowerCase();
     img.onload = function() {
        imgwidth = this.width;
        imgheight = this.height;
        console.log("imgwidth :"+imgwidth);
        console.log("imgheight :"+imgheight);
        if (imgwidth >= width && imgheight >= height) {
            $('#advertisement_image_error').addClass('d-none');
        } else {            
            $this.val('');
            $('#advertisement_image_error').removeClass('d-none');
            $("#advertisement_image_error").text("please select image which height is 100px and width is 350px");
            $("#file").attr('required','required');
        }
     };
     img.onerror = function() {

        $("#advertisement_image_error").text("not a valid file: " + file.type);
     }
});
</script>