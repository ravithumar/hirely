<!-- Start content -->
<div class="content">
<div class="container-fluid">
   <div class="row">
      <div class="col-12">
         <div class="page-title-box">
            <h4 class="page-title">Dashboard </h4>
            <ol class="breadcrumb m-0">
               <li class="breadcrumb-item"><a href="javascript: void(0);">Welcome to HIRELY admin panel !</a></li>
            </ol>
         </div>
      </div>
   </div>
   <?php $this->load->view('admin/includes/message');?>
   <div class="row">
      <div class="col-md-6 col-xl-3">
         <div class="widget-rounded-circle card-box">
            <div class="row">
               <div class="col-6">
                  <div class="avatar-lg rounded-circle bg-soft-info border-info border">
                     <i class="ti-user text-info font-22 avatar-title "></i>
                  </div>
               </div>
               <div class="col-6">
                  <div class="text-right">
                     <h3 class="text-dark mt-1"><span data-plugin="counterup"><?php echo isset($customer) ? $customer : 0; ?></span></h3>
                     <p class="text-muted mb-1"><?php echo __('Customer'); ?></p>
                  </div>
               </div>
            </div>
            <!-- end row-->
         </div>
         <!-- end widget-rounded-circle-->
      </div>
      <div class="col-md-6 col-xl-3">
         <div class="widget-rounded-circle card-box">
            <div class="row">
               <div class="col-6">
                  <div class="avatar-lg rounded-circle bg-soft-danger border-danger border">
                     <i class="ti-user text-danger font-22 avatar-title "></i>
                  </div>
               </div>
               <div class="col-6">
                  <div class="text-right">
                     <h3 class="text-dark mt-1"><span data-plugin="counterup"><?php echo isset($service_provider) ? $service_provider : 0; ?></span></h3>
                     <p class="text-muted mb-1"><?php echo __('Service Provider'); ?></p>
                  </div>
               </div>
            </div>
            <!-- end row-->
         </div>
         <!-- end widget-rounded-circle-->
      </div>
      <div class="col-md-6 col-xl-3">
         <div class="widget-rounded-circle card-box">
            <div class="row">
               <div class="col-6">
                  <div class="avatar-lg rounded-circle bg-soft-success border-success border">
                     <i class="ti-shopping-cart text-success font-22 avatar-title "></i>
                  </div>
               </div>
               <div class="col-6">
                  <div class="text-right">
                     <h3 class="text-dark mt-1"><span data-plugin="counterup"><?php echo isset($inprogress) ? $inprogress : 0; ?></span></h3>
                     <p class="text-muted mb-1"><?php echo __('Inprogress'); ?></p>
                  </div>
               </div>
            </div>
            <!-- end row-->
         </div>
         <!-- end widget-rounded-circle-->
      </div>
      <div class="col-md-6 col-xl-3">
         <div class="widget-rounded-circle card-box">
            <div class="row">
               <div class="col-6">
                  <div class="avatar-lg rounded-circle bg-soft-success border-success border">
                     <i class="ti-shopping-cart text-success font-22 avatar-title "></i>
                  </div>
               </div>
               <div class="col-6">
                  <div class="text-right">
                     <h3 class="text-dark mt-1"><span data-plugin="counterup"><?php echo isset($confirmed) ? $confirmed : 0; ?></span></h3>
                     <p class="text-muted mb-1"><?php echo __('Confirmed'); ?></p>
                  </div>
               </div>
            </div>
            <!-- end row-->
         </div>
         <!-- end widget-rounded-circle-->
      </div>
      <div class="col-md-6 col-xl-3">
         <div class="widget-rounded-circle card-box">
            <div class="row">
               <div class="col-6">
                  <div class="avatar-lg rounded-circle bg-soft-success border-success border">
                     <i class="ti-shopping-cart text-success font-22 avatar-title "></i>
                  </div>
               </div>
               <div class="col-6">
                  <div class="text-right">
                     <h3 class="text-dark mt-1"><span data-plugin="counterup"><?php echo isset($canceled) ? $canceled : 0; ?></span></h3>
                     <p class="text-muted mb-1"><?php echo __('Canceled'); ?></p>
                  </div>
               </div>
            </div>
            <!-- end row-->
         </div>
         <!-- end widget-rounded-circle-->
      </div>
      <div class="col-md-6 col-xl-3">
         <div class="widget-rounded-circle card-box">
            <div class="row">
               <div class="col-6">
                  <div class="avatar-lg rounded-circle bg-soft-success border-success border">
                     <i class="ti-shopping-cart-full text-success font-22 avatar-title "></i>
                  </div>
               </div>
               <div class="col-6">
                  <div class="text-right">
                     <h3 class="text-dark mt-1"><span data-plugin="counterup"><?php echo isset($finished_order) ? $finished_order : 0; ?></span></h3>
                     <p class="text-muted mb-1"><?php echo __('Finished Orders'); ?></p>
                  </div>
               </div>
            </div>
            <!-- end row-->
         </div>
         <!-- end widget-rounded-circle-->
      </div>
   </div>
</div>
