<link href="<?php echo assets('v3/libs/switchery/switchery.min.css');?>" rel="stylesheet" type="text/css" />
<div class="content">
    <div class="container-fluid">
        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <input type="button" onclick="$('#config_form').submit();" class="btn btn-default pull-right mt-2" value="<?php echo __('Save'); ?>">
                        
                    </div>
                    <h4 class="page-title"><?php echo __('System Configuration'); ?></h4>
                        <?php echo $this->breadcrumbs->show(); ?>
                </div>
            </div>
        </div>
        <?php $this->load->view('admin/includes/message'); ?>
        <div class="row">
            <div class="col-sm-12">
                <form id="config_form" enctype="multipart/form-data" action="<?php echo base_url('Admin/SystemController/save_system_config'); ?>" method="post" >
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                    <div class="portlet">
                        <div class="portlet-heading clearfix">
                            <h3 class="portlet-title">
                            <?php echo __('General Settings'); ?>
                            </h3>
                            <div class="portlet-widgets">
                                <a data-toggle="collapse" data-parent="#accordion1" href="#tab-general"><i class="ion-minus-round"></i></a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div id="tab-general" class="panel-collapse collapse show">
                            <div class="portlet-body">
                                <div class="row">
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="portlet ">
                        <div class="portlet-heading clearfix">
                            <h3 class="portlet-title">
                            <?php echo __('Notification & google map settings'); ?>
                            </h3>
                            <div class="portlet-widgets">
                                <a data-toggle="collapse" data-parent="#accordion1" href="#tab-notify-google"><i class="ion-minus-round"></i></a>
                            </div>
                            
                        </div>
                        <div id="tab-notify-google" class="panel-collapse collapse show">
                            <div class="portlet-body">
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label"><?php echo __('FCM key'); ?></label>
                                        <input type="text" value="<?php echo config('fcm_key'); ?>" class="form-control" name="fcm_key">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label"><?php echo __('Google map key'); ?></label>
                                        <input type="text" value="<?php echo config('map_key'); ?>" class="form-control" name="map_key">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="portlet ">
                        <div class="portlet-heading clearfix">
                            <h3 class="portlet-title">
                            <?php echo __('Email settings'); ?>
                            </h3>
                            <div class="portlet-widgets">
                                <a data-toggle="collapse" data-parent="#accordion1" href="#tab-email"><i class="ion-minus-round"></i></a>
                            </div>
                            
                        </div>
                        <div id="tab-email" class="panel-collapse collapse show">
                            <div class="portlet-body">
                                <div class="col-12 col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label"><?php echo __('From email'); ?></label>
                                        <input type="text" value="<?php echo config('from_email'); ?>" class="form-control" name="from_email">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label"><?php echo __('From name'); ?></label>
                                        <input type="text" value="<?php echo config('from_name'); ?>" class="form-control" name="from_name">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label"><?php echo __('Smtp host'); ?></label>
                                        <input type="text" value="<?php echo config('smtp_host'); ?>" class="form-control" name="smtp_host">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label"><?php echo __('Smtp port'); ?></label>
                                        <input type="text" value="<?php echo config('smtp_port'); ?>" class="form-control" name="smtp_port">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label"><?php echo __('Smtp user'); ?></label>
                                        <input type="text" value="<?php echo config('smtp_user'); ?>" class="form-control" name="smtp_user">
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label"><?php echo __('Smtp password'); ?></label>
                                        <input type="text" value="<?php echo config('smtp_pass'); ?>" class="form-control" name="smtp_pass">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <div class="portlet ">
                    <div class="portlet-heading clearfix">
                        <h3 class="portlet-title">
                        <?php echo __('Database backup'); ?>
                        </h3>
                        <div class="portlet-widgets">
                            <a data-toggle="collapse" data-parent="#accordion1" href="#tab-db"><i class="ion-minus-round"></i></a>
                        </div>
                        
                    </div>
                    <div id="tab-db" class="panel-collapse collapse show">
                        <div class="portlet-body">
                            <div class="col-12 col-md-6">
                                <table class="table">
                                    <tr>
                                        <th colspan="2" >
                                            <form   action="<?php echo base_url('Admin/SystemController/backup'); ?>" method="post" >
                                                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-primary">Click to take backup(<?php echo date('Y-m-d'); ?>)</button>
                                                </div>
                                            </form>
                                        </th>
                                    </tr>
                                    <tr>
                                        <th>Date</th>
                                        <th>Action</th>
                                    </tr>
                                    <?php
                                    foreach ($backup as $value) {
                                    ?>
                                    <tr>
                                        <td><?php echo date('Y-m-d',explode('.',$value)[0]); ?></td>
                                        <td>
                                            <a class="label label-default" href="<?php echo base_url('Admin/SystemController/download/'.$value); ?>"><i class="fa fa-download"></i></a>
                                            <a class="label label-danger" href="<?php echo base_url('Admin/SystemController/delete_backup/'.$value); ?>"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                    <?php
                                    }
                                    ?>
                                </table>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?php echo assets('v3/libs/switchery/switchery.min.js');?>"></script>
<script>
$('[data-plugin="switchery"]').each(function(a,e){
new Switchery($(this)[0],$(this).data())
});
</script>