<div class="content">
    <div class="container-fluid">
        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                       <input type="button" onclick="$('#config_form').submit();" class="btn btn-default pull-right clearfix" value="<?php echo __('Save'); ?>">
                    </div>
                    <h4 class="page-title"><?php echo __('App Configuration'); ?></h4>
                    <?php echo $this->breadcrumbs->show(); ?>
                </div>
            </div>
        </div>
        <?php $this->load->view('admin/includes/message'); ?>
        <div class="row">
            <div class="col-sm-12">
                <form id="config_form" enctype="multipart/form-data" action="<?php echo base_url('admin/SystemController/save_app_config'); ?>" method="post" >
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
                    <div class="portlet ">
                        <div class="portlet-heading clearfix">
                            <h3 class="portlet-title">
                            <?php echo __('User app maintenance & update settings'); ?>
                            </h3>
                            <div class="portlet-widgets">
                                <a data-toggle="collapse" data-parent="#accordion1" href="#customer-app"><i class="ion-minus-round"></i></a>
                            </div>
                            
                        </div>
                        <div id="customer-app" class="panel-collapse collapse show">
                            <div class="portlet-body">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label"><?php echo __('Android version'); ?></label>
                                        <input type="text" value="<?php echo $app[0]->version; ?>" class="form-control" name="android_customer_version">
                                        <div class="checkbox checkbox-custom m-t-10">
                                            <input id="android_customer_update" name="android_customer_update" type="checkbox"  <?php echo $app[0]->force_update==1?"checked":""; ?>>
                                            <label for="android_customer_update" class="text-danger">
                                                <?php echo __('Compulsory update'); ?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label"><?php echo __('IOS version'); ?></label>
                                        <input type="text" value="<?php echo $app[1]->version; ?>" class="form-control" name="ios_customer_version">
                                        <div class="checkbox checkbox-custom m-t-10">
                                            <input id="ios_customer_update" name="ios_customer_update" type="checkbox"  <?php echo $app[1]->force_update==1?"checked":""; ?>>
                                            <label for="ios_customer_update" class="text-danger">
                                                <?php echo __('Compulsory update'); ?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label"><?php echo __('Maintenance Mode'); ?></label>
                                        <div class="checkbox checkbox-custom m-t-10">
                                            <input id="customer_maintenance" name="customer_maintenance" type="checkbox"  <?php echo $app[1]->maintenance==1?"checked":""; ?>>
                                            <label for="customer_maintenance" class="text-danger">
                                                <?php echo __('Maintenance Mode Enable?'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    <div class="portlet ">
                        <div class="portlet-heading clearfix">
                            <h3 class="portlet-title">
                            <?php echo __('Provider app maintenance & update settings'); ?>
                            </h3>
                            <div class="portlet-widgets">
                                <a data-toggle="collapse" data-parent="#accordion1" href="#driver-app"><i class="ion-minus-round"></i></a>
                            </div>
                        </div>
                        <div id="driver-app" class="panel-collapse collapse show">
                            <div class="portlet-body">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-control-label"><?php echo __('Android version'); ?></label>
                                        <input type="text" value="<?php echo $app[2]->version; ?>" class="form-control" name="android_driver_version">
                                        <div class="checkbox checkbox-custom m-t-10">
                                            <input id="android_driver_update" name="android_driver_update" type="checkbox"  <?php echo $app[2]->force_update==1?"checked":""; ?>>
                                            <label for="android_driver_update" class="text-danger">
                                                <?php echo __('Compulsory update'); ?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label"><?php echo __('IOS version'); ?></label>
                                        <input type="text" value="<?php echo $app[3]->version; ?>" class="form-control" name="ios_driver_version">
                                        <div class="checkbox checkbox-custom m-t-10">
                                            <input id="ios_driver_update" name="ios_driver_update" type="checkbox"  <?php echo $app[3]->force_update==1?"checked":""; ?>>
                                            <label for="ios_driver_update" class="text-danger">
                                                <?php echo __('Compulsory update'); ?>
                                            </label>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="form-control-label"><?php echo __('Maintenance Mode'); ?></label>
                                        <div class="checkbox checkbox-custom m-t-10">
                                            <input id="driver_maintenance" name="driver_maintenance" type="checkbox"  <?php echo $app[3]->maintenance==1?"checked":""; ?>>
                                            <label for="driver_maintenance" class="text-danger">
                                                <?php echo __('Maintenance Mode Enable?'); ?>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
</div>