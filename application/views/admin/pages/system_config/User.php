<?php defined('BASEPATH') OR exit('No direct script access allowed');
use \Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Eloquent\SoftDeletes;
class User extends Eloquent{
	use SoftDeletes;
    protected $table = 'users';
    protected $fillable = ['name', 'email', 'mobile','image','address','gender','password','social','status'];
    protected $dates = ['deleted_at'];
    protected $softDelete = true;
    
    protected $hidden = array('created_at', 'updated_at','deleted_at','password_token','password');
    public static function createUser($request)
    {
    	$CI =& get_instance();
    	$CI->load->library('authentication');
    	$salt=$CI->authentication->generate_salt();
    	$password = $CI->authentication->generate_hash($request['password'], $salt);
    	$request['password']=$password;
        
    	$user=User::create($request);
    	$CI->db->insert('user_roles',array('user_id'=>$user->id,'role_id'=>$request['role']));
        return $user;
    }
    public static function createSocialUser($request)
    {
        $CI =& get_instance();
        $CI->load->library('authentication');
        $salt=$CI->authentication->generate_salt();
        // $password = $CI->authentication->generate_hash($request['password'], $salt);
        // $request['password']=$password;
        $user=User::create($request);
        $CI->db->insert('user_roles',array('user_id'=>$user->id,'role_id'=>$request['role']));
        return $user;
    }
    public static function updateUser($request,$id)
    {
        unset($request['role']);
        self::whereId($id)->update($request);
    }
    public function getImageAttribute($value) {
         if($this->social ==1)
         {
            return $value;
         }
         else
         {
             return ! is_null ($value) ? $value : base_url('assets/images/No_Image.png');
         }
    }
    public function getGenderAttribute($value) {
          return ! is_null ($value) ? $value : '';
    }
    public function getAddressAttribute($value) {
          return ! is_null ($value) ? $value : '';
    }
}
