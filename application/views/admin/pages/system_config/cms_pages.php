<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="page-title-box">
               <h4 class="page-title"><?php echo $main_title; ?></h4>
               <ol class="breadcrumb m-0">
                  <li class="breadcrumb-item"><a href="<?php echo $home; ?>"><?php echo $main_title; ?></a></li>                  
               </ol>
            </div>
         </div>
      </div>
      <!-- Page-Title -->      
      <?php $this->load->view('admin/includes/message'); ?>
      <div class="row">
         <div class="col-sm-6">
            <form id="cms_pages" enctype="multipart/form-data" action="<?php echo $action_path; ?>" method="post">
               <div class="portlet ">
                  <div class="portlet-heading clearfix">
                     <h3 class="portlet-title">
                        <?php echo $title; ?>
                     </h3>
                     <div class="portlet-widgets">
                        <a data-toggle="collapse" data-parent="#accordion1" href="#tab-email"><i class="ion-minus-round"></i></a>
                     </div>
                  </div>
                  <div id="tab-email" class="panel-collapse collapse show">
                     <div class="portlet-body">
                        <div class="col-12 col-md-12" >                           
                           <textarea class="note-codable" id="summernote-editor" role="textbox" aria-multiline="true" name="description"><?php echo $cms_pages->value; ?></textarea>                           
                           <!-- <div class="form-group">
                              <label class="form-control-label">Description</label>
                              <input type="text" class="form-control " name="description" data-parsley-required-message="Please Enter Description" required="" placeholder="Please Enter Description" value="<?php echo $cms_pages->value; ?>" >
                           </div>      -->
                           <input type="hidden" name="id" value="<?php echo $cms_pages->id; ?>">
                        </div>
                        <div class="row">
                           <div class="col-sm-12">
                              <div class="page-title-box">
                                 <div class="page-title-right">
                                    <input type="button" onclick="$('#cms_pages').submit();" class="btn btn-default pull-right mt-2 mb-2" value="<?php echo __('Update'); ?>">
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
