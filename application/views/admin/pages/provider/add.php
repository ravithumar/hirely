<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="page-title-box">
               <h4 class="page-title"><?php echo $main_title; ?></h4>
               <ol class="breadcrumb m-0">
                  <li class="breadcrumb-item"><a href="<?php echo $home; ?>"><?php echo $main_title; ?></a></li>
                  <li class="breadcrumb-item"><a href="javascript: void(0);">Add</a></li>
               </ol>
            </div>
         </div>
      </div>
      <!-- Page-Title -->      
      <?php $this->load->view('admin/includes/message'); ?>
      <div class="row">
         <div class="col-sm-6">
            <form id="service_provider_add" enctype="multipart/form-data" action="<?php echo base_url('admin/service-provider/add'); ?>" method="post">
               <div class="portlet ">
                  <div class="portlet-heading clearfix">
                     <h3 class="portlet-title">
                        <?php echo $title; ?>
                     </h3>
                     <div class="portlet-widgets">
                        <a data-toggle="collapse" data-parent="#accordion1" href="#tab-email"><i class="ion-minus-round"></i></a>
                     </div>
                  </div>
                  <div id="tab-email" class="panel-collapse collapse show">
                     <div class="portlet-body">
                        <div class="col-12 col-md-12">
                           <div class="form-group">
                              <label class="form-control-label"><?php echo __('Name'); ?></label>
                              <input type="text" class="form-control " name="name" data-parsley-required-message="Please Enter Name" required="" placeholder="Please Enter Name">
                           </div>
                           <div class="form-group">
                              <label class="form-control-label"><?php echo __('Email'); ?></label>
                              <input type="email" class="form-control " name="email" data-parsley-required-message="Please Enter Email" required="" placeholder="Please Enter Email" id="email" data-parsley-email="" data-parsley-email-message="Email Already Exists" data-parsley-trigger="focusout">
                           </div>
                           <div class="form-group">
                              <label class="form-control-label"><?php echo __('Phone'); ?></label>
                              <input type="text" class="form-control " name="phone" data-parsley-required-message="Please Enter Phone" required="" placeholder="Please Enter Phone" id="phone" data-parsley-phone="" data-parsley-phone-message="Phone Already Exists" data-parsley-trigger="focusout">
                           </div>
                           <div class="form-group">
                              <label class="form-control-label"><?php echo __('Zip Code'); ?></label>
                              <input type="text" class="form-control " name="zipcode" data-parsley-required-message="Please Enter Zip Code" required="" placeholder="Please Enter Zip Code">
                           </div>
                           <div class="form-group">
                              <label class="form-control-label"><?php echo __('Gender'); ?></label></br>
                              <div class="form-check form-check-inline">
                                 <input class="form-check-input" type="radio" name="gender" id="male" value="male" required="" data-parsley-errors-container="#gender_error"  data-parsley-required-message="Please Select Gender">
                                 <label class="form-check-label" for="male">Male</label>
                              </div>
                              <div class="form-check form-check-inline">
                                 <input class="form-check-input" type="radio" name="gender" id="female" value="female" required="" data-parsley-errors-container="#gender_error" data-parsley-required-message="Please Select Gender">
                                 <label class="form-check-label" for="female">Female</label>
                              </div>
                              <div id="gender_error"></div>
                           </div>
                           <div class="form-group">
                              <label class="form-control-label"><?php echo __('Type'); ?></label></br>
                              <div class="form-check form-check-inline">
                                 <input class="form-check-input" type="radio" name="type" id="freelancer" value="freelancer" required="" data-parsley-errors-container="#type_error" data-parsley-required-message="Please Select Type">
                                 <label class="form-check-label" for="freelancer">Freelancer</label>
                              </div>
                              <div class="form-check form-check-inline">
                                 <input class="form-check-input" type="radio" name="type" id="company" value="company" required="" data-parsley-errors-container="#type_error" data-parsley-required-message="Please Select Type">
                                 <label class="form-check-label" for="company">Company</label>
                              </div>
                              <div id="type_error"></div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-12">
                              <div class="page-title-box">
                                 <div class="page-title-right">
                                    <input type="button" onclick="$('#service_provider_add').submit();" class="btn btn-default pull-right mt-2 mb-2" value="<?php echo __('Save'); ?>">
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
