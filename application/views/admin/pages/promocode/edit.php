<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="page-title-box">
               <h4 class="page-title"><?php echo $main_title; ?></h4>
               <ol class="breadcrumb m-0">
                  <li class="breadcrumb-item"><a href="<?php echo $home; ?>"><?php echo $main_title; ?></a></li>
                  <li class="breadcrumb-item"><a href="javascript: void(0);">Edit</a></li>
               </ol>
            </div>
         </div>
      </div>
      <!-- Page-Title -->      
      <?php $this->load->view('admin/includes/message'); ?>
      <div class="row">
         <div class="col-sm-6">
            <form id="promocode_add" enctype="multipart/form-data" action="<?php echo base_url('admin/promocode/edit/'.$promocode->id); ?>" method="post">
               <div class="portlet ">
                  <div class="portlet-heading clearfix">
                     <h3 class="portlet-title">
                        <?php echo $title; ?>
                     </h3>
                     <div class="portlet-widgets">
                        <a data-toggle="collapse" data-parent="#accordion1" href="#tab-email"><i class="ion-minus-round"></i></a>
                     </div>
                  </div>
                  <div id="tab-email" class="panel-collapse collapse show">
                     <div class="portlet-body">
                        <div class="col-12 col-md-12">
                           <div class="form-group">
                              <label class="form-control-label">Code</label>
                              <input type="text" class="form-control " name="code" data-parsley-required-message="Please Enter Name" required="" value="<?php echo $promocode->code; ?>" placeholder="Please Enter Name" id="promocode" data-parsley-promocode="" data-parsley-promocode-message="Promocode Already Exists" data-parsley-trigger="focusout">
                              <input type="hidden" name="old_name" id="old_name" value="<?php echo $promocode->code; ?>">
                              <input type="hidden" class="form-control " name="id" value="<?php echo $promocode->id; ?>">
                           </div>
                           <div class="form-group">
                              <label>Start Date</label><br />
                              <fieldset class="form-group form-group-style">                                  
                                 <input type="date" id="" min="<?php echo date('Y-m-d'); ?>" class="form-control " name="start_date"  required="" data-parsley-required-message="Please Select Start Date" placeholder="mm/dd/yyyy" data-parsley-errors-container="#from_date_error" value="<?php echo $promocode->start_date; ?>">
                              </fieldset>
                              <div id="from_date_error"></div>
                           </div>
                           <div class="form-group">
                              <label>End Date</label><br />                                                                                 
                              <fieldset class="form-group form-group-style">                                  
                                 <input type="date" id="" min="<?php echo date('Y-m-d'); ?>" class="form-control " name="end_date"  required="" data-parsley-required-message="Please Select End Date" placeholder="mm/dd/yyyy" data-parsley-errors-container="#to_date_error" value="<?php echo $promocode->end_date; ?>">
                              </fieldset>
                              <div id="to_date_error"></div>
                           </div>
                           <div class="form-group">
                              <label>Discount Type</label><br />                                                                               
                              <select class="form-control" name="discount_type" data-parsley-required="true" data-parsley-errors-container="#discount-error" data-parsley-error-message="This value is required">
                                 <option value="">--- Select Discount Type ---</option>
                                 <option value="flat" <?php if ($promocode->discount_type == 'flat') {echo 'selected'; } ?>>Flat</option>
                                 <option value="percentage" <?php if ($promocode->discount_type == 'percentage') {echo 'selected'; } ?>>Percentage</option>
                              </select>
                              <label id="discount-error"></label>                              
                           </div>
                           <div class="form-group">
                              <label>Discount Value</label><br />                                                                               
                              <input type="text" class="form-control number_pnt" name="discount_value"  placeholder="Please Enter Discount Value"   data-parsley-required="true" data-parsley-errors-container="#discount_value-error" data-parsley-error-message="Please Enter Discount Value" value="<?php echo $promocode->discount_value; ?>">
                              <label id="discount-error"></label>                              
                           </div>
                           <div class="form-group">
                              <label>Usage Limit</label><br />                                             
                              <input type="text" class="form-control number" name="usage_limit"  placeholder="Please Enter Usage Limit"   data-parsley-required="true" data-parsley-error-message="Please Enter Usage Limit" value="<?php echo $promocode->usage_limit; ?>">
                           </div>
                           <div class="form-group">
                              <label>Description</label><br />            
                              <textarea class="form-control number" name="description"  placeholder="Please Enter Description"   data-parsley-required="true" data-parsley-error-message="Please Enter Description"><?php echo $promocode->description; ?></textarea>
                           </div>
                           <div class="form-group">
                              <label class="form-control-label"><?php echo __('Image'); ?></label>
                              <div class="custom-file">
                                 <input type="file" name="image"  class="custom-file-input parsley-error promocode_image" id="promocode_image" data-parsley-required-message="Please Select Image" data-parsley-errors-container="#promocode_image_error" accept="image/*" id="form_image" data-parsley-fileextension="jpg||png||jpeg||JPG||PNG||JPEG"  >
                                 <label class="custom-file-label" >Choose file</label>
                              </div>
                              <div class="" id="promocode_image_preview" style="margin-top: 5px;"> </div>
                              <div class="col-12 mt-2">
                                 <?php
                                    if (isset($promocode['image']) && $promocode['image'] != '')
                                    {
                                        if (file_exists($promocode['image']))
                                        {
                                            $img_src = BASE_URL() . $promocode['image'];
                                            $dis_attr = '';
                                            $image_class = "d-block";
                                        }
                                        else
                                        {
                                            $img_src = BASE_URL() . '/assets/images/default-profile.png';
                                            $dis_attr = 'disabled ';
                                            $image_class = "";
                                        }
                                    
                                    }
                                    else
                                    {
                                        $img_src = BASE_URL() . '/assets/images/default-profile.png';
                                        $dis_attr = 'disabled ';
                                        $image_class = "";
                                    }
                                    ?>
                                 <a href="<?php echo $img_src; ?>" class="image-popup">
                                 <img onerror="this.src='<?php echo base_url('assets/images/default-profile.png') ?>'" src="<?php echo $img_src; ?>" class="border rounded view_promocode_image_preview  preview <?php echo $image_class; ?>" alt="Category" id="" style="height: auto;max-width: 100px;" >  
                                 </a>
                                 <span class="promocode_image_error" id="promocode_image_error"></span>
                              </div>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-sm-12">
                              <div class="page-title-box">
                                 <div class="page-title-right">
                                    <input type="button" onclick="$('#promocode_add').submit();" class="btn btn-default pull-right mt-2 mb-2" value="<?php echo __('Save'); ?>">
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
