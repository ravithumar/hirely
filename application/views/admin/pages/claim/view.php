<div class="content">
   <div class="container-fluid">
      <div class="row">
         <div class="col-12">
            <div class="page-title-box">
               <h4 class="page-title"><?php echo $main_title; ?></h4>
               <ol class="breadcrumb m-0">
                  <li class="breadcrumb-item"><a href="<?php echo $home; ?>"><?php echo $main_title; ?></a></li>
                  <li class="breadcrumb-item"><a href="javascript: void(0);">View</a></li>
               </ol>
            </div>
         </div>
      </div>
      <!-- Page-Title -->
      <?php $this->load->view('admin/includes/message');?>
      <div class="row">
         <div class="col-sm-6 mt-2">
            <form id="promocode_add" enctype="multipart/form-data" action="<?php echo base_url('admin/claim/add'); ?>" method="post">
               <div class="portlet ">
                  <div class="portlet-heading clearfix">
                     <h3 class="portlet-title">
                        <?php echo $title; ?>
                     </h3>
                     <div class="portlet-widgets">
                        <a data-toggle="collapse" data-parent="#accordion1" href="#tab-email"><i class="ion-minus-round"></i></a>
                     </div>
                  </div>
                  <div id="tab-email" class="panel-collapse collapse show">
                     <div class="portlet-body">
                        <div class="col-12 col-md-12">
                           <div class="col-md-12 text-left">
                              <div class="table-responsive">
                                 <table class="table table-borderless">
                                    <tbody>
                                       <tr>
                                          <td class="font-weight-bold ">Title </td>
                                          <td class="font-weight-bold "> - </td>
                                          <td class="text-left "><?php echo isset($claim->reason) ? $claim->reason : ''; ?></td>
                                       </tr>
                                       <tr>
                                          <td class="font-weight-bold ">Description </td>
                                          <td class="font-weight-bold "> - </td>
                                          <td class="text-left "> <?php echo isset($claim->description) ? $claim->description : ''; ?></td>
                                       <tr>
                                          <td class="font-weight-bold ">Images </td>
                                          <td class="font-weight-bold "> - </td>
                                          <td class="text-left ">
                                             <div class="border border-light p-2 mb-3">
                                                <?php foreach (explode(', ', $claim->image) as $row) {?>
                                                <a href="<?php echo base_url($row); ?>" class="image-popup">
                                                   <img onerror="this.src='<?php echo base_url(claim) ?>'" src="<?php echo base_url($row); ?>" alt="post-img" class="rounded mr-1" height="60">
                                                </a>
                                                <?php }?>
                                             </div>
                                          </td>
                                       </tr>
                                       </tr>
                                       <tr>
                                          <td class="font-weight-bold ">Change Status </td>
                                          <td class="font-weight-bold "> - </td>
                                          <td class="text-left ">
                                             <?php if ($claim->active == 0) {
                                             	echo '<h5 class="mb-0 mt-0"><span onclick="fun_change_state(this);" class="btn btn-warning btn-xs waves-effect mb-2 waves-light status_' . $claim->id . '" data-table="' . $table . '" data-id="' . $claim->id . '">Pending</span></h5>';
                                             } else {
                                             	echo '<h5 class="mb-0 mt-0"><span onclick="fun_change_state(this);" class="btn btn-info btn-xs waves-effect mb-2 waves-light font-15 status_' . $claim->id . '" data-table="' . $table . '" data-id="' . $claim->id . '">In Process</span></h5>';
                                             }
                                             ?>
                                          </td>
                                       </tr>
                                    </tbody>
                                 </table>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
