<?php defined('BASEPATH') OR exit('No direct script access allowed');
class OwnerController extends MY_Controller {
	function __construct()
	{
		parent::__construct();
		$this->load->model('User');
		$this->load->model('Roles');
		$this->load->library('form_validation');
		
	}
	public function index()
    {
        // send_mail("Verify account",'developer.eww@gmail.com','register_temp',[]);
        // exit;
    	$columans=array(
    		__('Id') => "id",
    		__('Name') => "name",
    		__('Email') => "email"
    	);
    	$action['delete']=base_url('admin/owner/delete/');
    	$action['edit']=base_url('admin/owner/edit/');
        $action['view']=base_url('admin/owner/view/');
            // $action['image'] = true;
            // $action['path'] = assets('images/users/');
    	$table ="users";
    	$join['table_1']='user_roles';
    	$join['compare_1']='user_roles.user_id=users.id';
    	$join['table_2']='roles';
    	$join['compare_2']='user_roles.role_id=roles.id';
    	$join['columan']='users.id,users.name,users.email,users.created_at,users.image,users.status,roles.name as role';
        $where = array('user_roles.role_id'=>2);
    	__datatable_join_where($table,$columans,$action,true,true,$join,$where);
    	$data['title']=__('Owners');
    	$data['datatable']=true;	
    	$data['add_url']=base_url('admin/owner/add/');
        $this->render('owners/index',$data);
    }
    public function add()
    {
        $data['title']=__('Owner');
        $data['roles']=Roles::all();
        $data['role']= 2;
        $this->render('users/add',$data);
    }
     public function delete($id)
    {
        User::whereId($id)->delete();
        $this->session->set_flashdata('success', __('Owner details deleted successfully.'));
        redirect('admin/owners');    
    }
    public function edit($id)
    {
        $data['title']=__('Owner');
        $data['roles']=Roles::all();
        $data['role']= 2;
        $user=User::select('users.*','user_roles.role_id')
             ->join('user_roles','user_roles.user_id','users.id')
             ->where('user_id',$id)
             ->first();
        __is_empty($user);
        $data['user']=$user;
        $this->render('users/edit',$data);
    }
    public function view($id)
    {
        $data['title']=__('Owner');
        $data['roles']=Roles::all();
        $user=User::select('users.*','user_roles.role_id')
             ->join('user_roles','user_roles.user_id','users.id')
             ->where('user_id',$id)
             ->first();
        __is_empty($user);
        $data['user']=$user;
        $this->render('users/view',$data);
    }
    public function update_status($status,$user_id)
    {
         User::whereId($user_id)->update(array('status'=>$status));
         if($status == 1)
         {
            $this->session->set_flashdata('success', __('Owner profile activated.'));
            redirect('admin/owner/view/'.$user_id);    
         }
         else
         {
            $this->session->set_flashdata('success', __('Owner profile rejected.'));
            redirect('admin/owner/view/'.$user_id);    
         }
    }
}