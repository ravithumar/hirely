<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
        <meta name="description" content="<?php echo config('site_meta'); ?>">
        <meta name="keyword" content="<?php echo config('site_keyword'); ?>">
        <link rel="shortcut icon" href="<?php echo base_url('assets/images/favicon.png') ?>">
        <title><?php echo config('site_title'); ?></title>
        <link href="<?php echo assets('plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css');?>" rel="stylesheet">
        <link href="<?php echo assets('plugins/bootstrap-daterangepicker/daterangepicker.css');?>" rel="stylesheet">
        <link href="<?php echo assets('plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css')?>" rel="stylesheet" />
        <link href="<?php echo assets_css('bootstrap'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assets_css('icons'); ?>" rel="stylesheet" type="text/css" />
        <link href="<?php echo assets_css('style'); ?>" rel="stylesheet" type="text/css" />
        <script src="<?php echo assets_js('modernizr.min'); ?>"></script>
        <script src="<?php echo assets_js('jquery.min'); ?>"></script>
        <!-- Js form validations -->
        <script src="<?php echo assets_js('validation/jquery.validate'); ?>"></script>
        <script src="<?php echo assets_js('validation/jquery.validate.min'); ?>"></script>
        <script src="<?php echo assets_js('validation/additional-methods'); ?>"></script>
        <script src="<?php echo assets_js('validation/additional-methods.min'); ?>"></script>
        <script type="text/javascript">
        var BASE_URL = "<?php echo base_url(); ?>";
       
        </script>
    </head>
    <body class="fixed-left">
        <div id="wrapper">
            <!-- Top Bar Start -->
            <div class="topbar">
                <!-- LOGO -->
                <div class="topbar-left">
                    <div class="text-center logo-box">
                        <a href="<?php echo base_url('admin'); ?>" class="logo"><img class="preview_main" src="<?php echo base_url('assets/images/logo/'.config('site_logo')); ?>" style="height: 48px;object-fit: cover;"></a>
                    </div>
                </div>
                <nav class="navbar-custom">
                    <ul class="list-inline float-right mb-0">
                        <li class="list-inline-item dropdown notification-list">
                            <a style="color:#fff;font-size: 20px;" class="nav-link dropdown-toggle waves-effect waves-light nav-user" data-toggle="dropdown" href="#" role="button"
                                aria-haspopup="false" aria-expanded="false">
                                <i class="ti-user"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right profile-dropdown " aria-labelledby="Preview">
                                <!-- item-->
                                <div class="dropdown-item noti-title">
                                    <h5 class="text-overflow"><small>Welcome ! <?php echo $this->session->userdata('admin')['name'];?></small> </h5>
                                </div>
                                <a href="javascript:void(0);" class="dropdown-item notify-item" data-toggle="modal" data-target="#myModal">
                                    <i class="md  md-vpn-key"></i> <span><?php echo __('Change password') ?></span>
                                </a>
                                <a href="<?php echo base_url('auth/logout/admin'); ?>" class="dropdown-item notify-item">
                                    <i class="md md-settings-power"></i> <span>Logout</span>
                                </a>
                            </div>
                        </li>
                    </ul>
                    <ul class="list-inline menu-left mb-0">
                        <li class="float-left">
                            <button class="button-menu-mobile open-left waves-light waves-effect">
                            <i class="dripicons-menu"></i>
                            </button>
                        </li>
                    </ul>
                </nav>
            </div>
            <div id="myModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="myModalLabel"><?php echo __('Change password') ?></h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        </div>
                        <form action="<?php echo base_url('UserController/change_password/'.$this->session->userdata('identifier')) ?>" method="post" enctype="multipart/form-data">
                            <div class="modal-body">
                                
                                <div class="form-group">
                                    <label><?php echo __('Password') ?></label>
                                    
                                    <input type="password" id="reset_password"   class="form-control" name="password" parsley-trigger="change" required>
                                    
                                </div>
                                <div class="form-group">
                                    <label ><?php echo __('Confirm password') ?></label>
                                    <input type="password" data-parsley-equalto="#reset_password"  class="form-control"  parsley-trigger="change" required>
                                </div>
                                
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-default waves-effect waves-light">Save changes</button>
                            </div>
                        </form>
                        </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                        </div><!-- /.modal -->
                        
                        <?php $this->load->view('admin/layout/sidebar'); ?>
                        <div class="content-page">