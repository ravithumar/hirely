/*! codeigniter-sketch - v1.0.0 - 2018-08-22 */
function delete_confirm_(t) {
    swal({
        title: "Are you sure that you want to delete this record?",
        text: "You will not be able to recover this imaginary file!",
        type: "warning",
        showCancelButton: !0,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel plx!",
        closeOnConfirm: !1,
        closeOnCancel: !1
    }, function(e) {
        e ? (swal("Deleted!", "Your imaginary file has been deleted.", "success"), location.href = $(t).attr("data-href")) : swal("Cancelled", "Your imaginary file is safe :)", "error")
    })
}
window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function() {
        $(this).remove()
    })
}, 4e3), $(".reset").click(function() {
    window.history.back()
}), $(".confirm_model").click(function() {
    alert()
}), $(function() {
    $(document).on("change", ":file", function() {
        var e = $(this),
            t = e.get(0).files ? e.get(0).files.length : 1,
            n = e.val().replace(/\\/g, "/").replace(/.*\//, "");
        e.trigger("fileselect", [t, n])
    }), $(document).ready(function() {
        $(":file").on("fileselect", function(e, t, n) {
            console.log($(this).get(0).files);
            var i = $(this).parents(".input-group").find(":text"),
                l = 1 < t ? t + " files selected" : n;
            i.length && i.val(l)
        })
    }), $(":file").change(function() {
        ! function(e) {
            if (e.files && e.files[0]) {
                var t = new FileReader;
                t.onload = function(e) {
                    $(".preview").attr("src", e.target.result)
                }, t.readAsDataURL(e.files[0])
            }
        }(this)
    })
});

function delete_confirm(t) {
    var url         = $(t).data("href");
    var rediret_url = $(t).data("rediret-url");
    var error_msg   = $(t).data("error-msg");
    var table       = $(t).data("table");
    Swal.fire({
        title: "Are you sure that you want to delete this record?",
        text: "You will not be able to recover record!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: "#28D094",
        confirmButtonText: "Yes, delete it!",
        cancelButtonText: "No, cancel please!",
    }).then((result) => {
        if(table == "setting") {
            result.value ? (Swal.fire("Deleted!", "Your record has been deleted.", "success"), location.href = $(t).attr("data-href")) : Swal.fire("Cancelled", "Your record is safe :)", "error");
        } else {
            if (result.value) {
                if (window.XMLHttpRequest) {
                    xmlhttp = new XMLHttpRequest();
                } else {
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                  }
                xmlhttp.onreadystatechange = function() {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                        if(xmlhttp.responseText == 1) {
                            Swal.fire("Deleted!", "Your record has been deleted.", "success");
                             $(t).closest('tr').remove();
                            // location.href = rediret_url;
                        } else if(xmlhttp.responseText == 0) {
                            Swal.fire("Cancelled", "You can not delete this record due to "+error_msg+" :)", "error");
                        }
                    }
                };
                xmlhttp.open("GET",url,true);
                xmlhttp.send();
            } else {
//                swal.close();
                Swal.fire("Cancelled", "Your record is safe :)", "error");
            }
        }
    });
}
$('.change_status').click(function(){
    alert();
});
function fun_change_state(e)
{
    var table = $(e).data('table');
    var id = $(e).data('id');
    console.log("table : "+table);
    console.log("id :"+id);
    swal({
      title: "Are you sure that you want to change status of this record?",
      text: "",
      type: "warning",
      showCancelButton: true,
      confirmButtonClass: "btn-danger",
      confirmButtonText: "Yes, change it!",
      cancelButtonText: "No, cancel plx!",
      closeOnConfirm: false,
      closeOnCancel: false
    },
    function(isConfirm) {
      if (isConfirm) {
        swal("Status changed!", "status has been changed.", "success");
        if (window.XMLHttpRequest) {
            xmlhttp = new XMLHttpRequest();
        } else {
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                if(xmlhttp.responseText == 1)
                {
                    $('.status_'+id).html('Active');
                    $('.status_'+id).removeClass('badge-danger');
                    $('.status_'+id).addClass('badge-success');
                }
                else if(xmlhttp.responseText == 0)
                {
                    $('.status_'+id).html('Inactive');
                    $('.status_'+id).addClass('badge-danger');
                    $('.status_'+id).removeClass('badge-success');  
                }
            }
        };
        xmlhttp.open("GET",BASE_URL+'admin/change/status/'+table+'/'+id,true);
        xmlhttp.send();
      } else {
        swal("Cancelled", table+"   record status is safe :)", "error");
      }
    });
}
//========================IMAGE===================================
var imagesPreview = function(input, previewId) {    
    if (input.files) {
        var filesAmount = input.files.length;
        var preview;
        console.log(previewId);
        $('#'+previewId).html('');
        for (i = 0; i < filesAmount; i++) {
            var reader = new FileReader();
            reader.onload = function(event) {
                $('.view_'+previewId).removeClass('d-block');
                $('.view_'+previewId).addClass('d-none');
                
                preview = '<div style="margin-right:10px;margin-bottom:10px;"><img style="height: 110px;width: 110; border-radius: 6px;" src="'+event.target.result+'" ></div>';
                $('#'+previewId).append(preview);
            }
            reader.readAsDataURL(input.files[i]);
        }
    }
};
$('.category_image').on('change', function() {    
    console.log("category_image");
    imagesPreview(this, 'category_image_preview');
});

$('.service_image').on('change', function() {    
    console.log("service_image");
    imagesPreview(this, 'service_image_preview');
});

$('.advertisement_image').on('change', function() {    
    console.log("advertisement_image");
    imagesPreview(this, 'advertisement_image_preview');
});
$('.promocode_image').on('change', function() {    
    console.log("promocode_image");
    imagesPreview(this, 'promocode_image_preview');
});
//========================IMAGE===================================

//========================PROMOCODE===================================
$(document).ready(function() {
    $('#promocode').parsley();
    var old_value = $("#old_name").val();
    var table_name = 'promocode';
    var field_name = 'code';

    window.ParsleyValidator.addValidator('promocode', {
        validateString: function(value) {
            return $.ajax({
                url: BASE_URL + "check-duplicate-data",
                method: "POST",
                data: {
                    new_value: value,
                    old_value: old_value,
                    table_name: table_name,
                    field_name: field_name
                },
                dataType: "json",
                success: function(data) {
                    return true;
                }
            });
        }
    });
});
//========================PROMOCODE===================================

//========================NUMBER==========================================
$(document).ready(function() {
    //called when key is pressed in textbox
    $(".number").keypress(function(e) {
        //if the letter is not digit then display error and don't type anything
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57))

            return false;
        return true;

    });
});
//========================NUMBER==========================================

//========================NUMBER ( . )==========================================
$(document).ready(function() {
    //called when key is pressed in textbox
    $(".number_pnt").keypress(function(evt) {
        console.log('number_pnt');
        //if the letter is not digit then display error and don't type anything
        var iKeyCode = (evt.which) ? evt.which : evt.keyCode
        if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
            return false;
        return true;
    });
});
//========================NUMBER ( . )==========================================

//========================Service IMAGE==========================================

$('.remove_properties_more_image').on("click", function() {
    var this_tr = $(this).attr('id');
    console.log("this_tr: " + this_tr);
    var img_id = $(this).attr('id');
    console.log("img_id: " + img_id);    
    $.ajax({
        url: BASE_URL + "admin/service/remove-image",
        type: "POST",
        data: {
            img_id: img_id
        },
        success: function(returnData) {
            $('.remove_properties_more_image_' + img_id).hide();
            // window.setTimeout(function(){location.reload()},800)

            if (typeof returnData != "undefined") {}
        },
        error: function(xhr, ajaxOptions, thrownError) {
            console.log('error');
        }
    });
});

//========================Service IMAGE==========================================

//========================MAGNIFIC POPUP==========================================
$(document).ready(function() {
    $('form').parsley();
    $(".image-popup").magnificPopup({
        type: "image",
        closeOnContentClick: !1,
        closeBtnInside: !1,
        mainClass: "mfp-with-zoom mfp-img-mobile",
        image: {
            verticalFit: !0,
            titleSrc: function(e) {
                return e.el.attr("title")
            }
        },
        gallery: {
            enabled: !0
        },
        zoom: {
            enabled: !0,
            duration: 300,
            opener: function(e) {
                return e.find("img")
            }
        },
        callbacks: {
            elementParse: function(item) {
                // Function will fire for each target element
                // "item.el" is a target DOM element (if present)
                // "item.src" is a source that you may modify
                var source = item.src.split('.');
                if (source[source.length - 1] == "pdf") {
                    item.type = "iframe";
                }
                // console.log(item); // Do whatever you want with "item" object
            }
        }
    }), $(".filter-menu .filter-menu-item").click(function() {
        $(".filter-menu .filter-menu-item").removeClass("active"), $(this).addClass("active")
    }), $(function() {
        var e = "";
        $(".filter-menu-item").click(function() {
            e = $(this).attr("data-rel"), $(".filterable-content").fadeTo(100, 0), $(".filterable-content .filter-item").not("." + e).fadeOut().removeClass(""), setTimeout(function() {
                $("." + e).fadeIn().addClass(""), $(".filterable-content").fadeTo(300, 1)
            }, 300)
        })
    })
});
//========================MAGNIFIC POPUP==========================================